
#ifndef HMI_APP_H
#define HMI_APP_H

#include "tkc/types_def.h"
#include "tkc/thread.h"
#include "platforms/common/rtos.h"

ret_t hmi_app_start(const char* device);
ret_t hmi_app_stop(void);

#endif // HMI_APP_H

