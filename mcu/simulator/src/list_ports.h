
#ifndef LIST_PORTS_H
#define LIST_PORTS_H

#include <string>
#include <vector>

using std::string;
using std::vector;

/*!
 * Structure that describes a serial device.
 */
struct PortInfo {
  /*! Address of the serial port (this can be passed to the constructor of Serial). */
  std::string port;

  /*! Human readable description of serial device if available. */
  std::string description;

  /*! Hardware ID (e.g. VID:PID of USB serial devices) or "n/a" if not available. */
  std::string hardware_id;
};

#define UNICODE 1
#define _UNICODE 1

vector<PortInfo> list_ports(void);

#endif /*LIST_PORTS_H*/
