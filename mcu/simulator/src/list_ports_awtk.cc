/**
 * File:   hmilist_ports_awtk.h
 * Author: AWTK Develop Team
 * Brief:  list_ports_awtk
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-19 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#include "list_ports.h"
#include "list_ports_awtk.h"

extern "C" ret_t tk_list_ports(port_info_t* ports, uint32_t max_ports) {
  uint32_t i = 0;
  return_value_if_fail(ports != NULL, RET_BAD_PARAMS);
  return_value_if_fail(max_ports > 0, RET_BAD_PARAMS);
  memset(ports, 0x00, sizeof(port_info_t) * max_ports);

  vector<PortInfo> src_ports = list_ports();
  for (vector<PortInfo>::const_iterator iter = src_ports.begin(); iter != src_ports.end(); iter++, i++) {
    tk_strncpy(ports[i].name, iter->port.c_str(), TK_NAME_LEN);
    if (iter->description == "n/a") {
      tk_strncpy(ports[i].desc, iter->port.c_str(), TK_NAME_LEN);
    } else {
      tk_strncpy(ports[i].desc, iter->description.c_str(), TK_NAME_LEN);
    }
    if (i >= max_ports) {
      break;
    }
  }
  
  return RET_OK;
}
