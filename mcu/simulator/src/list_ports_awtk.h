/**
 * File:   hmilist_ports_awtk.h
 * Author: AWTK Develop Team
 * Brief:  list_ports_awtk
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-19 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#ifndef TK_LIST_PORTS_AWTK_H
#define TK_LIST_PORTS_AWTK_H

#include "tkc.h"

BEGIN_C_DECLS

/**
 * @class port_info_t
 * 串口信息。
*/
typedef struct _port_info_t {
    /**
     * @property {char*} name
     * 串口名称。
     */
    char name[TK_NAME_LEN + 1];
    /**
     * @property {char*} desc
     * 串口描述。
     */
    char desc[TK_NAME_LEN + 1];
} port_info_t;

/**
 * @method tk_list_ports
 * 列出所有串口。
 *
 * @param {port_info_t*} ports 串口信息数组。
 *
 * @param {uint32_t} max_ports 最大串口数量。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t tk_list_ports(port_info_t* ports, uint32_t max_ports);

END_C_DECLS

#endif /*TK_LIST_PORTS_AWTK_H*/