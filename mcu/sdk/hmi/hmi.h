/**
 * File:   hmi.h
 * Author: AWTK Develop Team
 * Brief:  hmi client
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-19 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#ifndef TK_HMI_H
#define TK_HMI_H

#ifdef TK_IS_PC
#define WITH_FULL_REMOTE_UI 1
#endif/*TK_IS_PC*/

#include "remote_ui/client/remote_ui.h"

BEGIN_C_DECLS

struct _hmi_t;
typedef struct _hmi_t hmi_t;

typedef ret_t (*hmi_on_prop_changed_t)(hmi_t* hmi, const char* name, const value_t* v);

/**
 * @class hmi_t
 * 串口屏客户端(供 MCU 使用)。
 */
struct _hmi_t {
  /**
   * @property {remote_ui_t*} remote_ui
   * @annotation ["readable"]
   * remote ui 对象。
   *> 高级用户可以使用此对象直接操作远程UI。
   */
  remote_ui_t* remote_ui;

  /*private*/
  hmi_on_prop_changed_t on_prop_changed;
  void* ctx;
};

/**
 * @method hmi_create
 * 创建hmi对象。
 *
 * @param {tk_iostream_t*} io 流对象。
 * @param {hmi_on_prop_changed_t} on_prop_changed 属性变化回调函数。
 * @param {void*} ctx 上下文。
 *
 * @return {hmi_t*} 返回hmi对象。
 */
hmi_t* hmi_create(tk_iostream_t* io, hmi_on_prop_changed_t on_prop_changed, void* ctx);

/**
 * @method hmi_create_with_serial
 * 创建hmi对象。
 *
 * @param {const char*} device 串口设备。
 * @param {hmi_on_prop_changed_t} on_prop_changed 属性变化回调函数。
 * @param {void*} ctx 上下文。
 *
 * @return {hmi_t*} 返回hmi对象。
 */
hmi_t* hmi_create_with_serial(const char* device, hmi_on_prop_changed_t on_prop_changed, void* ctx);

/**
 * @method hmi_set_prop_int
 * 设置整数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {int32_t} value 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop_int(hmi_t* hmi, const char* name, int32_t value);

/**
 * @method hmi_set_prop_str
 * 设置字符串属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {const char*} value 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop_str(hmi_t* hmi, const char* name, const char* value);

/**
 * @method hmi_set_prop_bool
 * 设置布尔属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {bool_t} value 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop_bool(hmi_t* hmi, const char* name, bool_t value);

/**
 * @method hmi_set_prop_float
 * 设置浮点数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {float} value 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop_float(hmi_t* hmi, const char* name, float value);

/**
 * @method hmi_set_prop_int64
 * 设置64位整数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {int64_t} value 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop_int64(hmi_t* hmi, const char* name, int64_t value);

/**
 * @method hmi_get_prop_int
 * 获取整数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {int32_t} defvalue 默认值。
 *
 * @return {int32_t} 返回属性值。
 */
int32_t hmi_get_prop_int(hmi_t* hmi, const char* name, int32_t defvalue);

/**
 * @method hmi_get_prop_bool
 * 获取布尔属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {bool_t} defvalue 默认值。
 *
 * @return {bool_t} 返回属性值。
 */
bool_t hmi_get_prop_bool(hmi_t* hmi, const char* name, bool_t defvalue);

/**
 * @method hmi_get_prop_float
 * 获取浮点数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {float} defvalue 默认值。
 *
 * @return {float} 返回属性值。
 */
float hmi_get_prop_float(hmi_t* hmi, const char* name, float defvalue);

/**
 * @method hmi_get_prop_str
 * 获取字符串属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {const char*} defvalue 默认值。
 *
 * @return {const char*} 返回属性值。
 */
const char* hmi_get_prop_str(hmi_t* hmi, const char* name, const char* defvalue);

/**
 * @method hmi_get_prop_int64
 * 获取64位整数属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} name 属性名称。
 * @param {int64_t} defvalue 默认值。
 *
 * @return {int64_t} 返回属性值。
 */
int64_t hmi_get_prop_int64(hmi_t* hmi, const char* name, int64_t defvalue);

/**
 * @method hmi_get_prop
 * 获取属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} target 目标对象。
 * @param {const char*} name 属性名称。
 * @param {value_t*} v 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_get_prop(hmi_t* hmi, const char* target, const char* name, value_t* v);

/**
 * @method hmi_set_prop
 * 设置属性。
 *
 * @param {hmi_t*} hmi hmi对象。
 * @param {const char*} target 目标对象。
 * @param {const char*} name 属性名称。
 * @param {const value_t*} v 属性值。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_set_prop(hmi_t* hmi, const char* target, const char* name, const value_t* v);

/**
 * @method hmi_dispatch
 * 处理事件。
 * @param {hmi_t*} hmi hmi对象。
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_dispatch(hmi_t* hmi);

/**
 * @method hmi_destroy
 * 销毁hmi对象。
 * @param {hmi_t*} hmi hmi对象。
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t hmi_destroy(hmi_t* hmi);

#define STR_GLOBAL_MODEL "global.model"

END_C_DECLS

#endif /*TK_HMI_H*/
