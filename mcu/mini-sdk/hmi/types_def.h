/**
 * File:   types_def.h
 * Author: AWTK Develop Team
 * Brief:  basic types definitions.
 *
 * Copyright (c) 2018 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-26 Li XianJing <xianjimli@hotmail.com> adapt from tkc
 *
 */

#ifndef TYPES_DEF_H
#define TYPES_DEF_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#define log_debug printf
#define ENSURE assert

#if defined(__cplusplus) && !defined(ARDUINO)
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

typedef uint8_t bool_t;

/**
 * @enum ret_t
 * @prefix RET_
 * @annotation ["scriptable"]
 * 函数返回值常量定义。
 */
typedef enum _ret_t {
  /**
   * @const RET_OK
   * 成功。
   */
  RET_OK = 0,
  /**
   * @const RET_OOM
   * Out of memory。
   */
  RET_OOM,
  /**
   * @const RET_FAIL
   * 失败。
   */
  RET_FAIL,
  /**
   * @const RET_NOT_IMPL
   * 没有实现/不支持。
   */
  RET_NOT_IMPL,
  /**
   * @const RET_QUIT
   * 退出。通常用于主循环。
   */
  RET_QUIT,
  /**
   * @const RET_FOUND
   * 找到。
   */
  RET_FOUND,
  /**
   * @const RET_BUSY
   * 对象忙。
   */
  RET_BUSY,
  /**
   * @const RET_REMOVE
   * 移出。通常用于定时器。
   */
  RET_REMOVE,
  /**
   * @const RET_REPEAT
   * 重复。通常用于定时器。
   */
  RET_REPEAT,
  /**
   * @const RET_NOT_FOUND
   * 没找到。
   */
  RET_NOT_FOUND,
  /**
   * @const RET_DONE
   * 操作完成。
   */
  RET_DONE,
  /**
   * @const RET_STOP
   * 停止后续操作。
   */
  RET_STOP,
  /**
   * @const RET_SKIP
   * 跳过当前项。
   */
  RET_SKIP,
  /**
   * @const RET_CONTINUE
   * 继续后续操作。
   */
  RET_CONTINUE,
  /**
   * @const RET_OBJECT_CHANGED
   * 对象属性变化。
   */
  RET_OBJECT_CHANGED,
  /**
   * @const RET_ITEMS_CHANGED
   * 集合数目变化。
   */
  RET_ITEMS_CHANGED,
  /**
   * @const RET_BAD_PARAMS
   * 无效参数。
   */
  RET_BAD_PARAMS,
  /**
   * @const RET_TIMEOUT
   * 超时。
   */
  RET_TIMEOUT,
  /**
   * @const RET_CRC
   * CRC错误。
   */
  RET_CRC,
  /**
   * @const RET_IO
   * IO错误。
   */
  RET_IO,
  /**
   * @const RET_EOS
   * End of Stream
   */
  RET_EOS,
  /**
   * @const RET_NOT_MODIFIED
   * 没有改变。
   */
  RET_NOT_MODIFIED,
  /**
   * @const RET_NO_PERMISSION
   * 没有权限。
   */
  RET_NO_PERMISSION,
  /**
   * @const RET_INVALID_ADDR
   * 无效地址。
   */
  RET_INVALID_ADDR,
  /**
   * @const RET_EXCEED_RANGE
   * 超出范围。
   */
  RET_EXCEED_RANGE,
  /**
   * @const RET_MAX_NR
   * 最大值。
   */
  RET_MAX_NR
} ret_t;

/**
 * @enum value_type_t
 * @prefix VALUE_TYPE_
 * @annotation ["scriptable"]
 * 类型常量定义。
 */
typedef enum _value_type_t {
  /**
   * @const VALUE_TYPE_INVALID
   * 无效类型。
   */
  VALUE_TYPE_INVALID = 0,
  /**
   * @const VALUE_TYPE_BOOL
   * BOOL类型。
   */
  VALUE_TYPE_BOOL,
  /**
   * @const VALUE_TYPE_INT8
   * int8_t类型。
   */
  VALUE_TYPE_INT8,
  /**
   * @const VALUE_TYPE_UINT8
   * uint8_t类型。
   */
  VALUE_TYPE_UINT8,
  /**
   * @const VALUE_TYPE_INT16
   * int16_t类型。
   */
  VALUE_TYPE_INT16,
  /**
   * @const VALUE_TYPE_UINT16
   * uint16_t类型。
   */
  VALUE_TYPE_UINT16,
  /**
   * @const VALUE_TYPE_INT32
   * int32_t类型。
   */
  VALUE_TYPE_INT32,
  /**
   * @const VALUE_TYPE_UINT32
   * uint32_t类型。
   */
  VALUE_TYPE_UINT32,
  /**
   * @const VALUE_TYPE_INT64
   * int64_t类型。
   */
  VALUE_TYPE_INT64,
  /**
   * @const VALUE_TYPE_UINT64
   * uint64_t类型。
   */
  VALUE_TYPE_UINT64,
  /**
   * @const VALUE_TYPE_FLOAT
   * float_t类型。
   */
  VALUE_TYPE_FLOAT,
  /**
   * @const VALUE_TYPE_DOUBLE
   * double类型。
   */
  VALUE_TYPE_DOUBLE,
  /**
   * @const VALUE_TYPE_STRING
   * char*类型。
   */
  VALUE_TYPE_STRING
} value_type_t;

/**
 * @class value_t
 * @order -9
 * @annotation ["scriptable"]
 * 一个通用数据类型，用来存放整数、浮点数、字符串和其它对象。
 *
 * 在C/C++中，一般不需动态创建对象，直接声明并初始化即可。如：
 *
 * ```c
 * value_t v;
 * value_set_int(&v, 100);
 * ```
 *
 *> 在脚本语言中，需要动态创建对象。
 *
 */
typedef struct _value_t {
  uint32_t type : 8;
  uint32_t free_handle : 1;
  union {
    bool_t b;
    int8_t i8;
    uint8_t u8;
    int16_t i16;
    uint16_t u16;
    int32_t i32;
    uint32_t u32;
    int64_t i64;
    uint64_t u64;
    uint32_t token;
    float f32;
    double f64;
    void* ptr;
    char* str;
  } value;
} value_t;

#ifndef TRUE
#define TRUE 1
#endif/*TRUE*/

#ifndef FALSE
#define FALSE 0
#endif/*FALSE*/

#define return_value_if_fail(p, ret) \
  if (!(p)) {                        \
    return (ret);                    \
  }

/**
 * @method value_set_bool
 * 设置类型为bool的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v value对象。
 * @param {bool_t}   value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_bool(value_t* v, bool_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_BOOL;
  v->value.b = value;
  return v;
}

/**
 * @method value_bool
 * 获取类型为bool的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {bool_t} 值。
 */
static inline bool_t value_bool(const value_t* v) {
  return_value_if_fail(v != NULL, FALSE);

  return v->value.b;
}

/**
 * @method value_set_int8
 * 设置类型为int8的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {int8_t}   value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_int8(value_t* v, int8_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_INT8;
  v->value.i8 = value;
  return v;
}

/**
 * @method value_int8
 * 获取类型为int8的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {int8_t} 值。
 */
static inline int8_t value_int8(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.i8;
}

/**
 * @method value_set_uint8
 * 设置类型为uint8的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {uint8_t}  value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_uint8(value_t* v, uint8_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_UINT8;
  v->value.u8 = value;
  return v;
}

/**
 * @method value_uint8
 * 获取类型为uint8的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {uint8_t} 值。
 */
static inline uint8_t value_uint8(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.u8;
}

/**
 * @method value_set_int16
 * 设置类型为int16的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {int16_t}  value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_int16(value_t* v, int16_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_INT16;
  v->value.i16 = value;
  return v;
}

/**
 * @method value_int16
 * 获取类型为int16的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {int16_t} 值。
 */
static inline int16_t value_int16(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.i16;
}

/**
 * @method value_set_uint16
 * 设置类型为uint16的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {uint16_t} value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_uint16(value_t* v, uint16_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_UINT16;
  v->value.u16 = value;
  return v;
}

/**
 * @method value_uint16
 * 获取类型为uint16的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {uint16_t} 值。
 */
static inline uint16_t value_uint16(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.u16;
}

/**
 * @method value_set_int32
 * 设置类型为int32的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {int32_t}  value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_int32(value_t* v, int32_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_INT32;
  v->value.i32 = value;
  return v;
}

/**
 * @method value_int32
 * 获取类型为int32的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {int32_t} 值。
 */
static inline int32_t value_int32(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.i32;
}

/**
 * @method value_set_uint32
 * 设置类型为uint32的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {uint32_t} value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_uint32(value_t* v, uint32_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_UINT32;
  v->value.u32 = value;
  return v;
}

/**
 * @method value_uint32
 * 获取类型为uint32的值。
 * @param {const value_t*} v value对象。
 *
 * @return {uint32_t} 值。
 */
static inline uint32_t value_uint32(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.u32;
}

/**
 * @method value_set_int64
 * 设置类型为int64的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {int64_t}  value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_int64(value_t* v, int64_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_INT64;
  v->value.i64 = value;
  return v;
}

/**
 * @method value_int64
 * 获取类型为int64的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {int64_t} 值。
 */
static inline int64_t value_int64(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.i64;
}

/**
 * @method value_set_uint64
 * 设置类型为uint64的值。
 * @annotation ["scriptable"]
 * @param {value_t*} v     value对象。
 * @param {uint64_t} value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_uint64(value_t* v, uint64_t value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_UINT64;
  v->value.u64 = value;
  return v;
}

/**
 * @method value_uint64
 * 获取类型为uint64的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {uint64_t} 值。
 */
static inline uint64_t value_uint64(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.u64;
}

/**
 * @method value_set_float
 * 设置类型为float的值。
 * @param {value_t*} v     value对象。
 * @param {float}    value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_float(value_t* v, float value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_FLOAT;
  v->value.f32 = value;
  return v;
}

/**
 * @method value_float
 * 获取类型为float的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {float} 值。
 */
static inline float value_float(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.f32;
}

/**
 * @method value_set_double
 * 设置类型为double的值。
 * @annotation ["scriptable"]
 * @alias value_set_float64
 * @param {value_t*} v     value对象。
 * @param {double}   value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_double(value_t* v, double value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_DOUBLE;
  v->value.f64 = value;
  return v;
}

/**
 * @method value_double
 * 获取类型为double的值。
 * @annotation ["scriptable"]
 * @alias value_float64
 * @param {const value_t*} v value对象。
 *
 * @return {double} 值。
 */
static inline double value_double(const value_t* v) {
  return_value_if_fail(v != NULL, 0);

  return v->value.f64;
}

/**
 * @method value_set_str
 * 设置类型为字符串的值。
 * @param {value_t*} v     value对象。
 * @param {const char*}   value 待设置的值。
 *
 * @return {value_t*} value对象本身。
 */
static inline value_t* value_set_str(value_t* v, const char* value) {
  return_value_if_fail(v != NULL, NULL);

  v->type = VALUE_TYPE_STRING;
  v->value.str = (char*)value;
  return v;
}

/**
 * @method value_str
 * 获取类型为字符串的值。
 * @annotation ["scriptable"]
 * @param {const value_t*} v value对象。
 *
 * @return {const char*} 值。
 */
static inline const char* value_str(const value_t* v) {
  return_value_if_fail(v != NULL, NULL);

  return v->value.str;
}

/**
 * @enum tk_msg_data_type_t
 * @prefix MSG_DATA_TYPE_
 * 数据类型。
*/
typedef enum _tk_msg_data_type_t {
  /**
   * @const MSG_DATA_TYPE_NONE
   * 无效数据类型。
   */
  MSG_DATA_TYPE_NONE = 0,
  /**
   * @const MSG_DATA_TYPE_UBJSON
   * JSON数据类型。
   */
  MSG_DATA_TYPE_UBJSON,
  /**
   * @const MSG_DATA_TYPE_STRING
   * 字符串数据类型。
   */
  MSG_DATA_TYPE_STRING,
  /**
   * @const MSG_DATA_TYPE_BINARY
   * 二进制数据类型。
   */
  MSG_DATA_TYPE_BINARY
} tk_msg_data_type_t;

/**
 * @enum tk_msg_code_t
 * @prefix MSG_
 * 消息类型。
 * 
 */
typedef enum _tk_msg_code_t {
  /**
   * @const MSG_NONE
   * 无效消息。
  */
  MSG_NONE = 0,
  /**
   * @const MSG_CODE_NOTIFY
   * 服务端主动通知。
   */
  MSG_CODE_NOTIFY,
  /**
   * @const MSG_CODE_CONFIRM
   * 数据包确认。
   */
  MSG_CODE_CONFIRM,
  /**
   * @const MSG_CODE_LOGIN
   * 登录请求。
   */
  MSG_CODE_LOGIN,
  /**
   * @const MSG_CODE_LOGOUT
   * 登出请求。
  */
  MSG_CODE_LOGOUT,
  /**
   * @const MSG_USER_START
   * 用户扩展消息起始值。
   */
  MSG_USER_START = 100
} tk_msg_code_t;

/**
 * @class tk_msg_header_t
 * 消息头。
*/
typedef struct _tk_msg_header_t {
  /**
   * @property {uint32_t} size
   * 消息体的大小。
   */
  uint32_t size;
  /**
   * @property {uint16_t} type
   * 消息类型。
   */
  uint16_t type;
  /**
   * @property {uint8_t} data_type
   * 数据类型。
   */
  uint8_t data_type;
  /**
   * @property {uint8_t} resp_code
   * 响应码(仅适用于resp)。
   */
  uint8_t resp_code;
} tk_msg_header_t;

/**
 * @enum remote_ui_msg_code_t
 * @prefix REMOTE_UI_
 * 消息类型。
 * 
 */
typedef enum _remote_ui_msg_code_t {
  /**
   * @const REMOTE_UI_GET_DEV_INFO
   * 获取设备信息。
   */
  REMOTE_UI_GET_DEV_INFO = MSG_USER_START,
  /**
   * @const REMOTE_UI_REBOOT
   * 重新加载请求。
   */
  REMOTE_UI_REBOOT,
  /**
   * @const REMOTE_UI_CREATE_DIR
   * 创建目录请求。
   */
  REMOTE_UI_CREATE_DIR,
  /**
   * @const REMOTE_UI_REMOVE_DIR
   * 删除目录请求。
   */
  REMOTE_UI_REMOVE_DIR,
  /**
   * @const REMOTE_UI_REMOVE_FILE
   * 删除文件请求。
   */
  REMOTE_UI_REMOVE_FILE,
  /**
   * @const REMOTE_UI_ON_EVENT
   * 注册事件请求。
   */
  REMOTE_UI_ON_EVENT,
  /**
   * @const REMOTE_UI_OFF_EVENT
   * 注销事件请求。
   */
  REMOTE_UI_OFF_EVENT,
  /**
   * @const REMOTE_UI_SEND_EVENT
   * 发送事件请求。
   */
  REMOTE_UI_SEND_EVENT,
  /**
   * @const REMOTE_UI_OPEN_WINDOW
   * 打开窗口请求。
   */
  REMOTE_UI_OPEN_WINDOW,
  /**
   * @const REMOTE_UI_OPEN_DIALOG
   * 打开基本对话框请求。
   */
  REMOTE_UI_OPEN_DIALOG,
  /**
   * @const REMOTE_UI_CLOSE_WINDOW
   * 关闭窗口请求。
   */
  REMOTE_UI_CLOSE_WINDOW,
  /**
   * @const REMOTE_UI_BACK_TO_PREV
   * 返回请求。
   */
  REMOTE_UI_BACK_TO_PREV,
  /**
   * @const REMOTE_UI_BACK_TO_HOME
   * 返回主页请求。
   */
  REMOTE_UI_BACK_TO_HOME,
  /**
   * @const REMOTE_UI_SET_PROP
   * 设置属性请求。
   */
  REMOTE_UI_SET_PROP,
  /**
   * @const REMOTE_UI_GET_PROP
   * 获取属性请求。
   */
  REMOTE_UI_GET_PROP,
  /**
   * @const REMOTE_UI_GET_XML_SOURCE
   * 获取xml源码请求。
   */
  REMOTE_UI_GET_XML_SOURCE,
  /**
   * @const REMOTE_UI_EXEC_FSCRIPT
   * 执行脚本请求。
   */
  REMOTE_UI_EXEC_FSCRIPT,

  /**
   * @const REMOTE_UI_MOVE_WIDGET
   * 移动控件请求。
  */
  REMOTE_UI_MOVE_WIDGET,
  /**
   * @const REMOTE_UI_RESIZE_WIDGET
   * 调整控件大小请求。
  */
  REMOTE_UI_RESIZE_WIDGET,
  /**
   * @const REMOTE_UI_CREATE_WIDGET
   * 创建控件请求。
  */
  REMOTE_UI_CREATE_WIDGET,
  /**
   * @const REMOTE_UI_DESTROY_WIDGET
   * 销毁控件请求。
  */
  REMOTE_UI_DESTROY_WIDGET,
} remote_ui_msg_code_t;

/**
 * @enum event_type_t
 * @annotation ["scriptable"]
 * @prefix EVT_
 * 类型常量定义。
 */
typedef enum _event_base_type_t {
  /** 
   * @const EVT_NONE
   * 无效事件名称。
   */
  EVT_NONE = 0,
  /** 
   * @const EVT_PROP_WILL_CHANGE
   * 对象的属性即将改变的事件名(prop_change_event_t)。
   */
  EVT_PROP_WILL_CHANGE,
  /**
   * @const EVT_PROP_CHANGED
   * 对象的属性改变的事件名(prop_change_event_t)。
   */
  EVT_PROP_CHANGED
} event_base_type_t;

END_C_DECLS

#endif /*TK_VALUE_H*/
