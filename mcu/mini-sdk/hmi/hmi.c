/**
 * File:   hmi.c
 * Author: AWTK Develop Team
 * Brief:  hmi client
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-26 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#include "hmi.h"

#define PPPINITFCS16 0xffff /* Initial FCS value */

static uint16_t fcstab_16[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf, 0x8c48, 0x9dc1, 0xaf5a, 0xbed3,
    0xca6c, 0xdbe5, 0xe97e, 0xf8f7, 0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876, 0x2102, 0x308b, 0x0210, 0x1399,
    0x6726, 0x76af, 0x4434, 0x55bd, 0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c, 0xbdcb, 0xac42, 0x9ed9, 0x8f50,
    0xfbef, 0xea66, 0xd8fd, 0xc974, 0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3, 0x5285, 0x430c, 0x7197, 0x601e,
    0x14a1, 0x0528, 0x37b3, 0x263a, 0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9, 0xef4e, 0xfec7, 0xcc5c, 0xddd5,
    0xa96a, 0xb8e3, 0x8a78, 0x9bf1, 0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70, 0x8408, 0x9581, 0xa71a, 0xb693,
    0xc22c, 0xd3a5, 0xe13e, 0xf0b7, 0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036, 0x18c1, 0x0948, 0x3bd3, 0x2a5a,
    0x5ee5, 0x4f6c, 0x7df7, 0x6c7e, 0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd, 0xb58b, 0xa402, 0x9699, 0x8710,
    0xf3af, 0xe226, 0xd0bd, 0xc134, 0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3, 0x4a44, 0x5bcd, 0x6956, 0x78df,
    0x0c60, 0x1de9, 0x2f72, 0x3efb, 0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a, 0xe70e, 0xf687, 0xc41c, 0xd595,
    0xa12a, 0xb0a3, 0x8238, 0x93b1, 0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330, 0x7bc7, 0x6a4e, 0x58d5, 0x495c,
    0x3de3, 0x2c6a, 0x1ef1, 0x0f78};

uint16_t tk_crc16_byte(uint16_t crc, uint8_t data) {
  return (crc >> 8) ^ fcstab_16[(crc ^ (data)) & 0xff];
}

uint16_t tk_crc16(uint16_t crc, const void* data, int data_length) {
  const uint8_t* pdata = (const uint8_t*)data;

  while (data_length) {
    crc = (crc >> 8) ^ fcstab_16[(crc ^ (*pdata)) & 0xff];
    pdata++;
    data_length--;
  }

  return crc;
}

/**************************************************/

static ret_t hmi_on_event(hmi_t* hmi);
static ret_t hmi_on_notify(hmi_t* hmi, tk_msg_header_t* header);

hmi_t* hmi_init(hmi_t* hmi, hmi_read_t read, hmi_write_t write,
                hmi_on_prop_changed_t on_prop_changed, void* ctx) {
  return_value_if_fail(hmi != NULL && read != NULL && write != NULL && on_prop_changed != NULL,
                       NULL);
  hmi->ctx = ctx;
  hmi->read = read;
  hmi->write = write;
  hmi->on_prop_changed = on_prop_changed;

  hmi_on_event(hmi);

  return hmi;
}

hmi_t* hmi_init_with_fd(hmi_t* hmi, hmi_read_t read, hmi_write_t write,
                        hmi_on_prop_changed_t on_prop_changed, int fd) {
  return_value_if_fail(hmi != NULL && read != NULL && write != NULL && on_prop_changed != NULL,
                       NULL);
  hmi->fd = fd;
  hmi->ctx = NULL;
  hmi->read = read;
  hmi->write = write;
  hmi->on_prop_changed = on_prop_changed;

  hmi_on_event(hmi);

  return hmi;
}

static uint32_t hmi_get_value_size(const value_t* v) {
  uint32_t size = 0;
  switch (v->type) {
    case VALUE_TYPE_BOOL:
      size = sizeof(bool_t);
      break;
    case VALUE_TYPE_INT8:
    case VALUE_TYPE_UINT8:
      size = sizeof(int8_t);
      break;
    case VALUE_TYPE_INT16:
    case VALUE_TYPE_UINT16:
      size = sizeof(int16_t);
      break;
    case VALUE_TYPE_INT32:
    case VALUE_TYPE_UINT32:
      size = sizeof(int32_t);
      break;
    case VALUE_TYPE_FLOAT:
      size = sizeof(float);
      break;
    case VALUE_TYPE_DOUBLE:
      size = sizeof(double);
      break;
    case VALUE_TYPE_STRING:
      size = strlen(value_str(v)) + 1;
      break;
    case VALUE_TYPE_INT64:
    case VALUE_TYPE_UINT64:
      size = sizeof(int64_t);
      break;
    default: {
      break;
    }
  }

  return size;
}

static ret_t hmi_write_crc(hmi_t* hmi) {
  return hmi->write(hmi, &(hmi->crc), 2) == 2 ? RET_OK : RET_FAIL;
}

static ret_t hmi_write_data(hmi_t* hmi, const void* data, uint32_t size) {
  return_value_if_fail(hmi != NULL && data != NULL && size > 0, RET_BAD_PARAMS);
  hmi->crc = tk_crc16(hmi->crc, data, size);

  return hmi->write(hmi, data, size) == size ? RET_OK : RET_FAIL;
}

static ret_t hmi_write_header(hmi_t* hmi, tk_msg_header_t* header) {
  return_value_if_fail(hmi != NULL && header != NULL, RET_BAD_PARAMS);

  hmi->crc = PPPINITFCS16;
  return hmi_write_data(hmi, header, sizeof(*header));
}

static ret_t hmi_write_data_bool(hmi_t* hmi, bool_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_int8(hmi_t* hmi, int8_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_uint8(hmi_t* hmi, uint8_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_int16(hmi_t* hmi, int16_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_uint16(hmi_t* hmi, uint16_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_int32(hmi_t* hmi, int32_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_uint32(hmi_t* hmi, uint32_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_int64(hmi_t* hmi, int64_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_uint64(hmi_t* hmi, uint64_t value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_float(hmi_t* hmi, float value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_double(hmi_t* hmi, double value) {
  return hmi_write_data(hmi, &value, sizeof(value));
}

static ret_t hmi_write_data_str(hmi_t* hmi, const char* value) {
  return hmi_write_data(hmi, (uint8_t*)value, strlen(value) + 1);
}

static ret_t hmi_write_value(hmi_t* hmi, const value_t* v) {
  ret_t ret = RET_FAIL;
  return_value_if_fail(hmi != NULL && v != NULL, RET_BAD_PARAMS);
  ret = hmi_write_data_uint8(hmi, v->type);
  return_value_if_fail(ret == RET_OK, ret);

  switch (v->type) {
    case VALUE_TYPE_BOOL: {
      ret = hmi_write_data_bool(hmi, value_bool(v));
      break;
    }
    case VALUE_TYPE_INT8: {
      ret = hmi_write_data_int8(hmi, value_int8(v));
      break;
    }
    case VALUE_TYPE_UINT8: {
      ret = hmi_write_data_uint8(hmi, value_uint8(v));
      break;
    }
    case VALUE_TYPE_INT16: {
      ret = hmi_write_data_int16(hmi, value_int16(v));
      break;
    }
    case VALUE_TYPE_UINT16: {
      ret = hmi_write_data_uint16(hmi, value_uint16(v));
      break;
    }
    case VALUE_TYPE_INT32: {
      ret = hmi_write_data_int32(hmi, value_int32(v));
      break;
    }
    case VALUE_TYPE_UINT32: {
      ret = hmi_write_data_uint32(hmi, value_uint32(v));
      break;
    }
    case VALUE_TYPE_INT64: {
      ret = hmi_write_data_int64(hmi, value_int64(v));
      break;
    }
    case VALUE_TYPE_UINT64: {
      ret = hmi_write_data_uint64(hmi, value_uint64(v));
      break;
    }
    case VALUE_TYPE_FLOAT: {
      ret = hmi_write_data_float(hmi, value_float(v));
      break;
    }
    case VALUE_TYPE_DOUBLE: {
      ret = hmi_write_data_double(hmi, value_double(v));
      break;
    }
    case VALUE_TYPE_STRING: {
      ret = hmi_write_data_str(hmi, value_str(v));
      break;
    }
    default:
      break;
  }

  return ret;
}

static ret_t hmi_read_header(hmi_t* hmi, tk_msg_header_t* header) {
  int32_t ret = 0;
  return_value_if_fail(hmi != NULL && header != NULL, RET_BAD_PARAMS);

  hmi->crc = PPPINITFCS16;
  memset(header, 0x00, sizeof(*header));
  ret = hmi->read(hmi, header, sizeof(*header));
  if (ret == sizeof(*header)) {
    hmi->crc = tk_crc16(hmi->crc, header, sizeof(*header));
  }

  return ret == sizeof(*header) ? RET_OK : RET_FAIL;
}

static ret_t hmi_read_resp_header(hmi_t* hmi, tk_msg_header_t* header) {
  ret_t ret = RET_FAIL;

  while (TRUE) {
    ret = hmi_read_header(hmi, header);
    if (ret != RET_OK) {
      return ret;
    }

    if (header->type == MSG_CODE_NOTIFY) {
      ret = hmi_on_notify(hmi, header);
      if (ret != RET_OK) {
        return ret;
      } else {
        continue;
      }
    } else {
      break;
    }
  }

  return ret;
}

static ret_t hmi_read_crc(hmi_t* hmi) {
  uint16_t crc = 0;
  int32_t ret = hmi->read(hmi, &crc, sizeof(crc));

  if (ret == 2 && crc == hmi->crc) {
    return RET_OK;
  }

  log_debug("crc error\n");
  return RET_CRC;
}

static ret_t hmi_read_data(hmi_t* hmi, void* data, uint32_t size) {
  int32_t ret = 0;
  return_value_if_fail(hmi != NULL && data != NULL && size > 0, RET_BAD_PARAMS);

  ret = hmi->read(hmi, data, size);
  if (ret > 0) {
    hmi->crc = tk_crc16(hmi->crc, data, ret);
  }

  return ret == size ? RET_OK : RET_FAIL;
}

static ret_t hmi_set_prop(hmi_t* hmi, const char* target, const char* name, const value_t* v) {
  ret_t ret = RET_OK;
  tk_msg_header_t header;

  return_value_if_fail(hmi != NULL && name != NULL && v != NULL, RET_BAD_PARAMS);

  header.resp_code = 0;
  header.type = REMOTE_UI_SET_PROP;
  header.data_type = MSG_DATA_TYPE_BINARY;
  header.size =
      strlen(target) + 1 /*0*/ + strlen(name) + 1 /*0*/ + 1 /*type*/ + hmi_get_value_size(v);

  ret = hmi_write_header(hmi, &header);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_str(hmi, target);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_str(hmi, name);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_value(hmi, v);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_crc(hmi);
  return_value_if_fail(ret == RET_OK, ret);

  memset(&header, 0x00, sizeof(header));
  ret = hmi_read_resp_header(hmi, &header);

  if (ret == RET_OK) {
    ret = header.resp_code;
  }

  if (hmi_read_crc(hmi) != RET_OK) {
    ret = RET_CRC;
  }

  return ret;
}

ret_t hmi_set_prop_int(hmi_t* hmi, const char* name, int32_t value) {
  value_t v;
  return_value_if_fail(hmi != NULL && name != NULL, RET_BAD_PARAMS);

  return hmi_set_prop(hmi, STR_GLOBAL_MODEL, name, value_set_int32(&v, value));
}

ret_t hmi_set_prop_str(hmi_t* hmi, const char* name, const char* value) {
  value_t v;
  return_value_if_fail(hmi != NULL && name != NULL && value != NULL, RET_BAD_PARAMS);

  return hmi_set_prop(hmi, STR_GLOBAL_MODEL, name, value_set_str(&v, value));
}

ret_t hmi_set_prop_bool(hmi_t* hmi, const char* name, bool_t value) {
  value_t v;
  return_value_if_fail(hmi != NULL && name != NULL, RET_BAD_PARAMS);

  return hmi_set_prop(hmi, STR_GLOBAL_MODEL, name, value_set_bool(&v, value));
}

ret_t hmi_set_prop_float(hmi_t* hmi, const char* name, float value) {
  value_t v;
  return_value_if_fail(hmi != NULL && name != NULL, RET_BAD_PARAMS);

  return hmi_set_prop(hmi, STR_GLOBAL_MODEL, name, value_set_float(&v, value));
}

ret_t hmi_set_prop_int64(hmi_t* hmi, const char* name, int64_t value) {
  value_t v;
  return_value_if_fail(hmi != NULL && name != NULL, RET_BAD_PARAMS);

  return hmi_set_prop(hmi, STR_GLOBAL_MODEL, name, value_set_int64(&v, value));
}

static const char* hmi_read_data_str(hmi_t* hmi) {
  char* p = NULL;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL, NULL);

  p = hmi->str;
  *p = '\0';

  while (TRUE) {
    ret = hmi_read_data(hmi, p, 1);
    return_value_if_fail(ret == RET_OK, NULL);
    if (*p == '\0') {
      break;
    }
    p++;
    if ((p - hmi->str) >= HMI_MAX_STR_SIZE) {
      return NULL;
    }
  }

  return hmi->str;
}

static ret_t hmi_read_value(hmi_t* hmi, value_t* v) {
  uint8_t type = 0;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL && v != NULL, RET_BAD_PARAMS);

  ret = hmi_read_data(hmi, &type, 1);
  return_value_if_fail(ret == RET_OK, ret);

  switch (type) {
    case VALUE_TYPE_BOOL: {
      bool_t value = FALSE;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_bool(v, value);
      break;
    }
    case VALUE_TYPE_INT8: {
      int8_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_int8(v, value);
      break;
    }
    case VALUE_TYPE_UINT8: {
      uint8_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_uint8(v, value);
      break;
    }
    case VALUE_TYPE_INT16: {
      int16_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_int16(v, value);
      break;
    }
    case VALUE_TYPE_UINT16: {
      uint16_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_uint16(v, value);
      break;
    }
    case VALUE_TYPE_INT32: {
      int32_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_int32(v, value);
      break;
    }
    case VALUE_TYPE_UINT32: {
      uint32_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_uint32(v, value);
      break;
    }
    case VALUE_TYPE_INT64: {
      int64_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_int64(v, value);
      break;
    }
    case VALUE_TYPE_UINT64: {
      uint64_t value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_uint64(v, value);
      break;
    }
    case VALUE_TYPE_FLOAT: {
      float value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_float(v, value);
      break;
    }
    case VALUE_TYPE_DOUBLE: {
      double value = 0;
      ret = hmi_read_data(hmi, &value, sizeof(value));
      return_value_if_fail(ret == RET_OK, ret);
      value_set_double(v, value);
      break;
    }
    case VALUE_TYPE_STRING: {
      const char* str = hmi_read_data_str(hmi);
      value_set_str(v, str);
      ret = str != NULL ? RET_OK : RET_FAIL;
      break;
    }
    default: {
      assert(!"not supported type");
      break;
    }
  }

  return ret;
}

static ret_t hmi_on_event(hmi_t* hmi) {
  ret_t ret = RET_OK;
  tk_msg_header_t header;
  const char* target = STR_GLOBAL_MODEL;
  return_value_if_fail(hmi != NULL, RET_BAD_PARAMS);

  header.resp_code = 0;
  header.type = REMOTE_UI_ON_EVENT;
  header.size = strlen(target) + 1 /*0*/ + 4 /*event*/;
  header.data_type = MSG_DATA_TYPE_BINARY;

  ret = hmi_write_header(hmi, &header);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_str(hmi, target);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_int32(hmi, EVT_PROP_CHANGED);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_crc(hmi);
  return_value_if_fail(ret == RET_OK, ret);

  memset(&header, 0x00, sizeof(header));
  ret = hmi_read_resp_header(hmi, &header);
  if (ret == RET_OK) {
    ret = header.resp_code;
  }
  hmi_read_data_str(hmi);

  if (hmi_read_crc(hmi) == RET_OK) {
    return ret;
  } else {
    return RET_CRC;
  }
}

ret_t hmi_logout(hmi_t* hmi) {
  ret_t ret = RET_OK;
  tk_msg_header_t header;
  return_value_if_fail(hmi != NULL, RET_BAD_PARAMS);

  header.size = 0;
  header.resp_code = 0;
  header.type = MSG_CODE_LOGOUT;
  header.data_type = MSG_DATA_TYPE_BINARY;

  ret = hmi_write_header(hmi, &header);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_crc(hmi);
  return_value_if_fail(ret == RET_OK, ret);

  memset(&header, 0x00, sizeof(header));
  ret = hmi_read_resp_header(hmi, &header);
  ret = hmi_read_crc(hmi);

  if (ret == RET_OK) {
    ret = header.resp_code;
  }

  return_value_if_fail(ret == RET_OK, ret);

  return ret;
}

ret_t hmi_get_prop(hmi_t* hmi, const char* target, const char* name, value_t* v) {
  ret_t ret = RET_OK;
  tk_msg_header_t header;

  return_value_if_fail(hmi != NULL && target != NULL && name != NULL, RET_BAD_PARAMS);

  header.resp_code = 0;
  header.type = REMOTE_UI_GET_PROP;
  header.size = strlen(target) + 1 /*0*/ + strlen(name) + 1 /*0*/;
  header.data_type = MSG_DATA_TYPE_BINARY;

  ret = hmi_write_header(hmi, &header);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_str(hmi, target);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_data_str(hmi, name);
  return_value_if_fail(ret == RET_OK, ret);

  ret = hmi_write_crc(hmi);
  return_value_if_fail(ret == RET_OK, ret);

  memset(&header, 0x00, sizeof(header));
  ret = hmi_read_resp_header(hmi, &header);

  if (ret == RET_OK) {
    ret = header.resp_code;
  }

  if (ret == RET_OK) {
    ret = hmi_read_value(hmi, v);
  } else {
    /*不修改ret*/
    hmi_read_value(hmi, v);
  }

  if (hmi_read_crc(hmi) == RET_CRC) {
    return RET_CRC;
  }

  return ret;
}

int32_t hmi_get_prop_int(hmi_t* hmi, const char* name, int32_t defvalue) {
  value_t v;
  ret_t ret = RET_OK;

  return_value_if_fail(hmi != NULL && name != NULL, defvalue);

  ret = hmi_get_prop(hmi, STR_GLOBAL_MODEL, name, &v);
  return_value_if_fail(ret == RET_OK, defvalue);

  return value_int32(&v);
}

bool_t hmi_get_prop_bool(hmi_t* hmi, const char* name, bool_t defvalue) {
  value_t v;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL && name != NULL, defvalue);

  ret = hmi_get_prop(hmi, STR_GLOBAL_MODEL, name, &v);
  return_value_if_fail(ret == RET_OK, defvalue);

  return value_bool(&v);
}

float hmi_get_prop_float(hmi_t* hmi, const char* name, float defvalue) {
  value_t v;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL && name != NULL, defvalue);

  ret = hmi_get_prop(hmi, STR_GLOBAL_MODEL, name, &v);
  return_value_if_fail(ret == RET_OK, defvalue);

  return value_float(&v);
}

const char* hmi_get_prop_str(hmi_t* hmi, const char* name, const char* defvale) {
  value_t v;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL && name != NULL, defvale);

  ret = hmi_get_prop(hmi, STR_GLOBAL_MODEL, name, &v);
  return_value_if_fail(ret == RET_OK, defvale);

  return value_str(&v);
}

int64_t hmi_get_prop_int64(hmi_t* hmi, const char* name, int64_t defvalue) {
  value_t v;
  ret_t ret = RET_OK;
  return_value_if_fail(hmi != NULL && name != NULL, defvalue);

  ret = hmi_get_prop(hmi, STR_GLOBAL_MODEL, name, &v);
  return_value_if_fail(ret == RET_OK, defvalue);

  return value_int64(&v);
}

static ret_t hmi_on_notify(hmi_t* hmi, tk_msg_header_t* header) {
  value_t v;
  ret_t ret = RET_OK;
  char target[16] = {0};
  char name[64] = {0};
  int32_t type = 0;
  const char* p = NULL;
  value_set_int32(&v, 0);
  return_value_if_fail(hmi != NULL && header != NULL, RET_BAD_PARAMS);

  p = hmi_read_data_str(hmi);
  return_value_if_fail(p != NULL, RET_FAIL);
  strncpy(target, p, sizeof(target) - 1);

  ret = hmi_read_data(hmi, &type, sizeof(type));
  return_value_if_fail(ret == RET_OK, RET_FAIL);
  ENSURE(type == EVT_PROP_CHANGED);

  p = hmi_read_data_str(hmi);
  return_value_if_fail(p != NULL, RET_FAIL);
  strncpy(name, p, sizeof(name) - 1);

  if (hmi_read_value(hmi, &v) != RET_OK) {
    return RET_FAIL;
  }

  if (hmi_read_crc(hmi) != RET_OK) {
    return RET_CRC;
  }

  if (hmi->on_prop_changed != NULL) {
    hmi->on_prop_changed(hmi, name, &v);
  }

  return RET_OK;
}

ret_t hmi_dispatch(hmi_t* hmi) {
  tk_msg_header_t header;
  return_value_if_fail(hmi != NULL, RET_BAD_PARAMS);

  memset(&header, 0x00, sizeof(header));
  if (hmi_read_header(hmi, &header) != RET_OK) {
    return RET_TIMEOUT;
  }

  ENSURE(header.type == MSG_CODE_NOTIFY);
  hmi_on_notify(hmi, &header);

  return RET_OK;
}

ret_t hmi_deinit(hmi_t* hmi) {
  return_value_if_fail(hmi != NULL, RET_BAD_PARAMS);

  hmi_logout(hmi);
  memset(hmi, 0x00, sizeof(*hmi));

  return RET_OK;
}
