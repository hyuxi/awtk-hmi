/**
 * File:   awtk_hmi_demo.ino
 * Author: AWTK Develop Team
 * Brief:  awtk hmi demo
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-11-30 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#include "hmi.c"

static hmi_t hmi;
static int s_value = 0;
#define STR_PROP_TEMP "温度"

static int32_t arduino_read(hmi_t *hmi, void *data, uint32_t size) {
  return Serial.readBytes((char *)data, (size_t)size);
}

static int32_t arduino_write(hmi_t *hmi, const void *data, uint32_t size) {
  return Serial.write((const char *)data, (size_t)size);
}

static ret_t arduino_on_prop_changed(hmi_t *hmi, const char *name, const value_t *v) {
  if (strcmp(name, STR_PROP_TEMP) == 0) {
    s_value = value_int32(v);
  }

  return RET_OK;
}

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  hmi_init(&hmi, arduino_read, arduino_write, arduino_on_prop_changed, NULL);
}

// the loop function runs over and over again forever
void loop() {
  if (Serial.available() > 10) {
    hmi_dispatch(&hmi);
  } else {
    s_value++;
    if (s_value > 100) {
      s_value = 0;
    }

    hmi_set_prop_int(&hmi, STR_PROP_TEMP, s_value);
    delay(100);
  }
}
