# MCU

## 目录介绍

### sdk 

MCU SDK 源代码。

除了提供 HMI 几个基本函数，提供了大量实用函数，方便 MCU 端的应用程序开发。加上 TKC 代码并不多，比如：

STM32F103：

```
Program Size: Code=46084 RO-data=1532 RW-data=740 ZI-data=63164  
```

STM32F429

```
Program Size: Code=49054 RO-data=1974 RW-data=704 ZI-data=156976  
```

### mini-sdk 

极小 SDK，不依赖 TKC。

### simulator 

PC 端的 MCU 模拟器，可以模拟发送和接受数据。

### stm32

提供了 STM32F103/STM32F429/STM32H741 等例子。

> 由于硬件原因，STM32F103串口通信未经验证。

## 用法

完善示例

```c

/*本文必须保存为 UTF-8 BOM 格式 */

#include "tkc/mem.h"
#include "hmi/hmi.h"

static int s_value = 0;
static uint32_t s_heap_mem[10240];

#define HMI_PROP_TEMP "温度"

/*回调函数*/
static ret_t hmi_on_prop_change(hmi_t* hmi, const char* name, const value_t* v) {
  if (strcmp(name, "温度") == 0) {
    s_value = value_int(v);
  }

  return RET_OK;
}

int main(void) {
  u8 key = 0;
  hmi_t* hmi = NULL;

  system_init();

  /*初始化内存*/
  tk_mem_init(s_heap_mem, sizeof(s_heap_mem));

  /*创建 HMI 对象*/
  hmi = hmi_create_with_serial("1", hmi_on_prop_change, NULL);

  while (1) {
    key = KEY_Scan(0);
    if (key) {
      switch (key) {
        case KEY2_PRES: {
          s_value = 0;
          break;
        }
        case KEY1_PRES: {
          s_value--;
          break;
        }
        case KEY0_PRES: {
          s_value++;
          break;
        }
        default: {
          break;
        }
      }

      /*修改数据*/
      hmi_set_prop_int(hmi, HMI_PROP_TEMP, s_value);
    } else {
      delay_ms(10);
    }

    /*分发事件*/
    hmi_dispatch(hmi);
  }
}
```
## 移植

* uart_hal.c 中的串口读写，需要实际平台修改。

## 注意

* Keil 编译需要勾选 C99。
