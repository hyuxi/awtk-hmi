# AWTK 开源智能串口屏方案

基于 [AWTK](https://github.com/zlgopen/awtk) 和 [AWTK-MVVM](https://github.com/zlgopen/awtk-mvvm) 实现的串口屏方案。

![image](docs/images/hmi_arch.png)

> 界面修改数据，自动通知 MCU。

![](docs/images/home_automation1.gif)

> MCU 修改数据，自动更新界面。

![](docs/images/home_automation2.gif)

## 1. 主要特色：

### 1.1 开发
 * 强大的界面设计器 [AWStudio](https://awtk.zlg.cn/awstudio/download.html)。
 * 基于 [AWTK](https://github.com/zlgopen/awtk) 实现强大的 GUI 功能（多窗口、输入法、动画和各种控件）。
 * 基于 [AWTK-MVVM](https://github.com/zlgopen/awtk-mvvm) 实现低代码开发（编写绑定规则即可实现常见应用程序）。
 * 支持在 PC 上模拟运行，并提供 MCU 模拟器模拟与串口屏的交互。
 * 开发时支持通过串口更新 UI 资源，无需插拔 USB (TODO)。
 * 开放源码，免费商用，从底层到应用程序全程可控。
 * 轻松集成第三方开源库，比如 sqlite3 和各种网络协议。

### 1.2 通信
 * 支持串口。
 * 支持 TCP。
 * 开发者无需了解通信协议。
 * 可以方便的移植到任何基于流的通信协议。 
 
### 1.3 MCU 端

提供简单易用的 API，无需了解通信协议，无需记忆变量地址，一般使用下面 4 个函数即可：

* 通过名称**设置**数据的值。
* 通过名称**获取**数据的值。
* 处理**数据变化**的事件。
* 在主循环中分发事件。

> 对于高级用户，也提供了一些直接操作 GUI 的函数。

也可以集成 [TKC](https://github.com/zlgopen/tkc)，[TKC](https://github.com/zlgopen/tkc) 中提供大量实用函数，可以加快 MCU 端嵌入式软件的开发。 

## 2. 配套硬件

本方案不限制硬件，能运行 AWTK-MVVM 即可。后面的例子可以 PC 上运行，同时也提供了基于
[立功科技 ZDP1440 HMI 显示驱动芯片](https://zlgmcu.com/intelligentchip/intelligentchip/product/id/231.html) 环境。

## 3. 使用方法

### 3.1 下载并编译 AWTK/AWTK-MVVM

* Windows 运行 prepare.bat

* Linux/MacOS 运行 prepare.sh

### 3.2 编译 MCU 模拟器

* 编译 MCU 模拟器

```sh
cd mcu/simulator
scons
python scripts/update_res.py all
```

* 运行 MCU 模拟器
```sh
./bin/mcu_sim 
```

### 3.3 编译 HMI Demo

* 编译 HMI Demo

```sh
cd hmi/demo_home2
scons
python scripts/update_res.py all
```

* 运行 HMI Demo

```sh
./bin/demo
```

## 4. 开发文档

### 4.1 演示程序的文档
 * [演示：Hello Word](docs/hello_word.md)
 * [演示：数据持久化(掉电保持)](docs/persistent.md)
 * [演示：按钮的三种用法](docs/button.md)
 * [演示：系统设置](docs/settings.md)
 * [演示：智能家居](docs/home_automation.md)
 * [演示：告警信息基本用法](docs/log_message.md)
 * [演示：告警信息高级用法](docs/log_message_ex.md)
 * [演示：数据采样](docs/history_data.md)
 * [演示：定时器](docs/timer.md)
 * [演示：屏幕保护](docs/screen_saver.md)
 * [演示：用户和权限管理](docs/user_manager.md)
 * [演示：天气预报](docs/weather.md)
 * [演示：记事本](docs/notepad.md)
 * [演示：计算器](docs/calculator.md)
 * [演示：界面重用](docs/ui_reuse.md)
 
### 4.2 内置模型的文档 
 * [默认 模型](docs/default_model.md)
 * [告警信息 模型](docs/log_message_model.md)
 * [采集数据 模型](docs/history_data_model.md)
 * [用户管理 模型](docs/user_manager_model.md)
 * [审计日志 模型](docs/command_audit_model.md)
  
### 4.3 开发环境搭建
 * [AWTK 开发及调试环境搭建](https://z.zlg.cn/articleinfo?id=853202)
 * [HMI_ZDP1440D 开发环境搭建](docs/hmi_zdp1440d_env.md) 

 ### 4.4 视频教学
* [AWTK 开源智能串口屏介绍](https://www.bilibili.com/video/BV1Ne411o7uf/)

* [AWTK 串口屏开发(1) - Hello World](https://www.bilibili.com/video/BV1bN4y147H1)

* [AWTK 串口屏开发(2) - 数据持久化](https://www.bilibili.com/video/BV1p94y1F7Ke)

* [AWTK 串口屏开发(3) - 按钮的3种用法](https://www.bilibili.com/video/BV1qN4y1z7qz)

* [AWTK 串口屏开发(4) - 数据绑定的高级用法](https://www.bilibili.com/video/BV1TQ4y1g7jB)

* [AWTK 串口屏开发(5)- 告警信息](https://www.bilibili.com/video/BV1B64y1J7Qq)

* [AWTK 串口屏开发(6)- 告警信息的高级用法](https://www.bilibili.com/video/BV1ge411i7wu)


## 相关产品

* [图像显示应用芯片ZMP110X](https://www.zlg.cn/microelectronics/microelectronics/product/id/290.html)

* [M6708双核/四核Cortex®-A9工控核心板](https://www.zlg.cn/ipc/ipc/product/id/89.html)

* [国产芯MR6450高性能RISC-V核心板](https://www.zlg.cn/ipc/ipc/product/id/323.html)

* [M1106/M1107系列核心板](https://www.zlg.cn/ipc/ipc/product/id/319.html)
