# # AWTK 开源串口屏开发 - 天气预报

天气预报是一个很常用的功能，在很多设备上都有这个功能。实现天气预报的功能，不能说很难但是也绝不简单，首先需要从网上获取数据，再解析数据，最后更新到界面上。

在 AWTK 串口屏中，内置了 XML/JSON/INI 等各种数据文件的模型，并支持用 HTTP/HTTPS 从网络获取数据。所以实现天气预报非常简单，不用编写一行代码即可实现天气预报的功能。而且用同样的方式，也可以实现其它功能，比如：股票行情、新闻、公交查询、火车查询、航班查询、快递查询等等。

这里以 http://t.weather.sojson.com 提供的接口为例，来实现一个显示天气信息的应用程序。这个是免费的，无需注册的 API，当然也有些限制，在实际工作中，你可以换成自己的接口。

它返回的数据是 JSON 格式的，如下所示：

```json
{
    "message": "success 感谢又拍云 (upyun.com) 提供 CDN 赞助",
    "status": 200,
    "date": "20240101",
    "time": "2024-01-01 08:13:13",
    "cityInfo": {
        "city": "天津市",
        "citykey": "101030100",
        "parent": "天津",
        "updateTime": "08:01"
    },
    "data": {
        "shidu": "86%",
        "pm25": 57.0,
        "pm10": 93.0,
        "quality": "良",
        "wendu": "-7",
        "ganmao": "极少数敏感人群应减少户外活动",
        "forecast": [
。
        ],
        "yesterday": {
            "date": "31",
            "high": "高温 1℃",
            "low": "低温 -3℃",
            "ymd": "2023-12-31",
            "week": "星期日",
            "sunrise": "07:30",
            "sunset": "16:57",
            "aqi": 35,
            "fx": "北风",
            "fl": "2 级",
            "type": "晴",
            "notice": "愿你拥有比阳光明媚的心情"
        }
    }
}
```

AWTK 串口屏中的 [json 模型](https://gitee.com/zlgopen/awtk-mvvm/blob/master/docs/view_model_conf.md)，支持用下面的语法从网络获取数据，它会自动解析 JSON 数据，并生成一个模型，通过路径可以引用模型中的数据。

```
json(url=http://t.weather.sojson.com/api/weather/city/101030100)
```

> 网上有很多天气预报的接口，这里只是举个例子，如果你有自己的接口，可以用自己的接口。

## 1. 功能

不用编写代码，实现天气预报。

## 2. 创建项目

从模板创建项目，将 hmi/template_app 拷贝 hmi/weather 即可。

> 第一个项目最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

用 AWStudio 打开上面 weather 目录下的 project.json 文件。里面有一个空的窗口，做出类似下面的界面。

![](images/weather1.png)

## 4. 添加绑定规则

### 4.1 城市

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {cityInfo.city} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.2 温度

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {data.wendu} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.3 湿度

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {data.shidu} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.4 PM2.5

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {data.pm25} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.5 空气质量

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {data.quality} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.6 感冒指数

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {data.ganmao} | 不同的 JSON API 需要使用不同的路径，请根据具体的 JSON 数据填写 |

### 4.7 刷新按钮

* 将 **刷新** 按钮的 **点击** 事件绑定到 **reload** 命令。添加自定义的属性 **v-on:click**，将值设置为 **{reload}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {reload} | reload 命令是内置的命令，用于重新加载持久化的配置，命令要用英文大括号括起来。 |

### 4.8 窗口模型

* 指定窗口的模型为 json, url 为 http://t.weather.sojson.com/api/weather/city/101030100

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model| json(url=http://t.weather.sojson.com/api/weather/city/101030100) | 不同的 JSON API 需要使用不同的 URL，可以换成自己的 URL|

## 5. 初始化数据

无

## 6. 描述需要持久化的数据

无

## 7. 编译运行 

运行 bin 目录下的 demo 程序：

![](images/weather0.png)

> 点击 **刷新** 按钮，可以重新加载数据，但是通常界面没有变化，因为天气数据没有变化。
 
## 8. 注意

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。

* 完整示例请参考：demo_weather
