# 通讯方式

## PC 端 (Linux/Windows/MacOS) 和嵌入式 Linux

默认使用 TCP 通讯，端口号为`2345`，可通过环境变量 **REMOTE_UI_URL** 修改:

```bash
export REMOTE_UI_URL=tcp://localhost:1234
```

```bash
export REMOTE_UI_URL=serial:///dev/ttys013
```

## 嵌入式 

使用串口通讯，可以通过定义宏 HMI\_SERVICE\_DEFAULT\_UART 修改默认串口。

