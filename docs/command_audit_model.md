# 命令审计日志 模型

命令审计日志 用于记录 **谁 (username) **在什么**时间 (time) **执行了什么**命令 (command)**。

名称：command_audit

功能：管理**审计日志**。提供对**审计日志** 的查看和查询等功能。

## 内置属性

| 属性 | 类型 | 说明 |
| :-----| ---- | :---- |
| query.start_date | 字符串 | 查询时用的 "开始日期"，格式为 "年/月/日" |
| query.start_time | 字符串 | 查询时用的 "开始时间"，格式为 "时：分：秒" |
| query.end_date | 字符串 | 查询时用的 "结束日期"，格式为 "年/月/日" |
| query.end_time | 字符串 | 查询时用的 "结束时间"，格式为 "时：分：秒" |
| query.username| 字符串| 查询时用的 "用户名" |
| query.command| 字符串 | 查询时用的 "命令" |

## 数据字段

| 字段 | 类型 | 说明 |
| :-----| ---- | :---- |
| time | 字符串 | 时间 |
| username | 字符串 | 用户名 |
| command | 字符串 | 命令名 |
| args | 字符串 | 命令名参数 |

## 内置命令

| 命令 | 参数 | 说明 |
| :-----| ---- | :---- |
| query | 参数为 clear 时清除查询 | 查询 |
