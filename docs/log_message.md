# AWTK 开源串口屏开发 - 告警信息

## 1. 功能

告警信息是一个常用的功能，MCU 在设备异常时，会发送告警信息到串口屏，串口屏可以显示告警信息，也可以对告警信息进行管理（保存或清除）。

![](images/log_message0.png)

基本工作原理：

* 1.MCU 端设置属性名为 **log_message**，数据类型为**字符串**，数据格式为 "告警级别|时间|设备|告警信息"数据。

* 2. 串口屏收到数据后，会把告警信息放到一个名为** log_message **的模型（数据）中。

* 3. 界面通过绑定规则将 **log_message** 模型中的数据关联到控件上。

> 告警级别可以是：调试 (0); 信息 (1); 警告 (2); 错误 (3)

> 告警信息中如果出现 "|" 字符，则整个告警信息需用用英文双引号引起来。

![](images/log_message1.png)

> 时间为 epoch 时间，方便内部存储和查询。

下面演示一下具体的实现方法。

## 2. 创建项目

从模板创建项目，将 hmi/template\_app 拷贝 hmi/log\_message 即可。

> 第一个项目最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

用 AWStudio 打开上面 log\_message 目录下的 project.json 文件。里面有一个空的窗口，在上面设计类似下面的界面：

![](images/log_message2.png)

> 中间是一个**列表视图**，**列表视图**中放一个**列表项**，**列表项**中放 5 个**文本控件**，分别用来显示序数、告警级别、时间、设备、告警信息。

## 3. 添加绑定规则

第一次用到列表视图，有几点需要特别说明一下：

列表视图中的**滚动视图**需要指定 **v-for-items** 属性：

| 属性 | 值 | 说明 |
| :-----| ---- | :---- |
| v-for-items | true | 它保证其下的列表项，会根据数据自动生成｜

### 3.0.1 几个特殊的变量

* index 特指序数。
* item 特指当前的数据。比如在这里 'item.level' 表示告警级别，'item.time' 表示时间，'item.device' 表示设备，'item.message' 表示告警信息。
* selected_index 表示当前选中的序数（可在**列表视图**之外绑定）。
* items 表示当前列表视图中的数据个数（可在**列表视图**之外绑定）。

### 3.0.2 几个特殊的命令

* set_selected 设置当前选中的序数（在列表项中使用）。
* save 保存数据到文件（在**列表视图**之外的按钮上绑定）。
* reload 重新加载数据（在**列表视图**之外的按钮上绑定）。
* clear 清除所有数据（在**列表视图**之外的按钮上绑定）。
* remove 删除指定序数的数据（在**列表视图**之外的按钮上绑定）。

### 3.1 序数

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {index} | index 特指序数。|

### 3.2 告警级别

前面提到告警级别是正数，可以通过 item.level 来获取它。它的意义对应为：调试 (0); 信息 (1); 警告 (2); 错误 (3)，我们需要用 one_of 函数将它转换为对应的字符串。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {one_of('调试;信息;警告;错误', item.level)} | 这里的 [one_of](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#422-one_of) 的功能是从指定的字符串数组中取出对应的子串。 |

### 3.3 时间

时间是整数(秒数)，可以通过 item.time 来获取。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {date_time_format(item.time, 'Y-M-D h:m:s')} | 需要用[date_time_format](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_date_time.md#12date_time_format)将 epoch 时间转换成人类可读的时间。|


### 3.4 设备

设备是一个字符串，可以通过 item.device 来获取。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {item.device} | |

### 3.5 告警信息

告警信息是一个字符串，可以通过 item.message 来获取。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {item.message} | |

### 3.6 列表项

为了配合删除选中的告警信息，需要在列表项加两个绑定规则。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {set_selected} | 点击时将当前项目设置为选中 |
| v-data:focused | {index==selected_index} | 当前项目选中时高亮 |

### 3.7 删除当前选择的告警信息

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {remove, Args=selected_index} | selected_index 表示当前选中的项目 |

### 3.8 清除所有告警信息

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {clear} | |

### 3.9 保存告警信息

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {save} | |

### 3.10 重新加载告警信息

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {reload} | |

### 3.11 退出应用程序

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {nothing, QuitApp=true} | |

### 3.12 指定窗口的模型

* 指定窗口的模型为 **log_message**

![](images/log_message3.png)

## 4. 启用告警信息

修改 design/default/data/settings.json 文件，启用告警信息：

```json
{
    "name": "hmi_log_message1",
    "log_message": {
        "enable": true, /*是否启用告警信息*/
        "fields": [
            "level" /*告警级别*/,
            "time" /*日期时间*/,
            "device" /*设备*/,
            "message" /*信息*/
        ],
        "fields_seperator": "|", /*字段之间的分隔符*/
        "max_rows": 1000 /*告警信息最大行数*/
    }
}
```

## 5. 编译运行 

运行 bin 目录下的 demo 程序。

![](images/log_message0.png)

## 6. 使用 MCU 模拟器与之进行交互

运行 mcu/simulator 目录下的 mcu_sim 程序，连接到 Localhost:2233。

* 通过模拟器发送数据，可以看到串口屏界面，自动添加告警信息。

![](images/log_message.gif)

测试数据：

```
1|1702030398|大门|长时间没有关闭
```

## 7. 注意

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。

* 实际使用时，在 demo\_log\_message1/design/default/ui/home\_page.xml 基础上进行调整即可，无需重复上面的过程，但是最好了解其中的原理。
