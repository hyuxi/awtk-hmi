# AWTK 开源串口屏开发 - 屏幕保护

现代屏幕其实并不需要屏幕保护，不过屏幕保护程序会衍生一些其它用途。比如：

* 保护隐私。长时间不操作，通过动画或者其它方式隐藏屏幕内容。
* 数据安全。长时间不操作，需要输入密码才能恢复。
* 美观/广告。长时间不操作，显示动画或者播放视频广告。

本文介绍一下在 AWTK 串口屏中，是如何实现屏幕保护的。基本工作原理是这样的：

* 长时间没有用户输入事件，触发屏幕保护事件。
* 在屏幕保护事件中，打开名为 screen\_saver 的窗口。
* screen\_saver 窗口中，显示屏保内容，如果收到输入事件，关闭该窗口(或要求输入密码)。 

## 1. 功能

在这里例子中，模型（也就是数据）里只有一个 **screen\_saver\_time** 变量：

| 变量名 | 数据类型 | 功能说明 |
| :-----| ---- | :---- |
| screen\_saver\_time | 整数 | 单位为毫秒 |

> screen\_saver\_time 是 [默认模型](default_model.md) 中一个内置属性。

## 2. 创建项目

从模板创建项目，将 hmi/template_app 拷贝 hmi/screen_saver 即可。

> 第一个项目最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

### 3.1 主窗口

用 AWStudio 打开上面 screen_saver 目录下的 project.json 文件。里面有一个空的窗口，在上面加入下面的控件：

* 静态文本
* 编辑器

做出类似下面的界面。

![](images/screen_saver0.png)

### 3.2 屏保窗口

在这个窗口中，我们通过一个定时器来改变窗口的背景颜色，创建一个空白窗口，将其改名为 screen_saver 即可。

## 4. 添加绑定规则

### 4.1 主窗口

* 将 **编辑器** 绑定到 **screen\_saver\_time** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{screen\_saver\_time}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value | {screen\_saver\_time} | 变量要用英文大括号括起来。 |

* 同样指定窗口的模型为 default。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model| default | default 不需要用大括号括起来。 |

### 4.2 屏保窗口

* 在 **启动** 按钮的 **点击** 事件启动定时器。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:window_open | {fscript, Args=start_timer(3000);set（计数，0)} |  启动定时器 |
| v-on:timer| {fscript, Args=set（计数，计数+1)} |  增加计数 |
| v-data:style:normal:bg_color| {one_of('red;blue;green;gold;orange;white;black', 计数%7)} |  根据**计数**改变背景颜色 |
| v-on:pointer_move |  {nothing, CloseWindow=true} | 关闭窗口 |
| v-on:pointer_up | {nothing, CloseWindow=true}|  关闭窗口 |
| v-on:key_up | {nothing, CloseWindow=true}|  关闭窗口 |

> 这里只是用了一个窗口内局部的变量“计数”，不需要指定模型，系统会自动创建一个 dummy 模型。

## 4. 初始化数据

修改资源文件 design/default/data/default_model.json, 将其内容改为：

```json
{
  "screen_saver_time": 180000
}
```

注意：

* 如果文件内容有中文（非 ASCII 字符），一定要保存为 UTF-8 格式。

* 重新打包资源才能生效。

## 5. 数据持久化

为了保存屏保时间，修改资源文件 design/default/data/settings.json, 将其内容改为：

```json
{
    "name":"hmi_screen_saver",
    "persistent" : {
      "screen_saver_time": true
    }
}
```

## 6. 编译运行 

运行 bin 目录下的 demo 程序，设置屏幕时间为一个较短的值，等待屏保启动。

![](images/screen_saver0.png)

![](images/screen_saver1.gif)

 
## 7. 注意

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。

* 完整示例请参考：demo_screen_saver
