# AWTK 开源串口屏开发 - 定时器的用法

定时器也是一个常用的功能，在 AWTK 串口屏中，可以给每个控件（包括窗口）创建一个定时器，定时器到点后触发 v-on:timer 事件。在 AWTK 串口屏中，提供了 6 个定时器相关函数：

* [start_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#514-start_timer) 启动定时器。

* [stop_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#515-stop_timer) 停止定时器。

* [suspend_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#518-suspend_timer) 暂停定时器。

* [resume_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#519-resume_timer) 恢复暂停的定时器。

* [reset_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#516-reset_timer) 重置定时器。

* [modify_timer](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript_widget.md#517-modify_timer) 修改定时器的时间。

这些函数第一个参数为控件的名字，如果不指定，则默认为当前控件。

## 1. 功能

本文以计数器的例子来介绍**定时器**的使用方法。在这里例子中，模型（也就是数据）里只有一个**计数**变量：

| 变量名 | 数据类型 | 功能说明 |
| :-----| ---- | :---- |
| 计数 | 整数 | 无|

## 2. 创建项目

从模板创建项目，将 hmi/template_app 拷贝 hmi/timer 即可。

> 第一个项目最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

用 AWStudio 打开上面 timer 目录下的 project.json 文件。里面有一个空的窗口，在上面加入下面的控件：

* 静态文本
* 4 个按钮

做出类似下面的界面。

![](images/timer0.png)

> 定时器设置在静态文本上，所以必须给它取一个在当前窗口内唯一的名称，这里就叫 "counter"。

## 4. 添加绑定规则

* 将 **静态文本** 绑定到 **计数** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{计数}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value | {计数} | 变量要用英文大括号括起来。 |
| v-on:timer | {fscript, Args=set(计数，计数+1)} | 定时器事件，增加计数|

* 在 **启动** 按钮的 **点击** 事件启动定时器。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=start_timer('counter', 1000);} | 1000 表示定时器的时间间隔为 1000 毫秒。  |

* 在 **暂停** 按钮的 **点击** 事件暂停定时器。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=suspend_timer('counter')} | 无  |

* 在 **恢复** 按钮的 **点击** 事件恢复定时器。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=resume_timer('counter')} | 无  |

* 在 **停止** 按钮的 **点击** 事件停止定时器。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=stop_timer('counter');set（计数，0)} | 停止定时器，并将计数清零  |

* 同样指定窗口的模型为 default。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model| default | default 不需要用大括号括起来。 |

## 5. 初始化数据

修改资源文件 design/default/data/default_model.json, 将其内容改为：

```json
{
  "计数":0
}
```

注意：

* 如果文件内容有中文（非 ASCII 字符），一定要保存为 UTF-8 格式。

* 重新打包资源才能生效。

## 6. 编译运行 

运行 bin 目录下的 demo 程序，启动定时器后，计数自动增加。

![](images/timer1.png)

![](images/timer1.gif)

 
## 7. 注意

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。
  
* 完整示例请参考：demo_timer
