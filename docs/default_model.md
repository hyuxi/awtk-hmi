# 默认模型

名称：default

默认模型是系统内置的模型，用于提供系统的基本功能。

## 内置属性

| 属性 | 类型 | 说明 |
| :-----| ---- | :---- |
| app_name | string | 应用名称 |
| build_date | string | 编译日期 |
| build_time | string | 编译时间 |
| rtc_year | int | RTC 年 |
| rtc_month | int | RTC 月 |
| rtc_day | int | RTC 日 |
| rtc_hour | int | RTC 时 |
| rtc_minute | int | RTC 分 |
| rtc_second | int | RTC 秒 |
| rtc_week | int | RTC 星期 |
| rtc_ymd | string | RTC 年月日，格式为 YYYY-MM-DD 或 YYYY/MM/DD |
| rtc_hms | string | RTC 时分秒，格式为 HH:MM:SS |
| backlight | int | 背光亮度 (0-100) |
| audio_volume | int | 音量 (0-100) |
| ui_feedback | bool | UI 反馈。true 启用反馈(beep), false 禁用反馈 |
| screen\_saver\_time |int| 屏幕保护时间，单位为毫秒|
| font_scale | float | 字体缩放比例[0.5, 2]|

## 内置命令

| 命令 | 参数 | 说明 |
| :-----| ---- | :---- |
| set_rtc | 无 | 将属性 rtc_year, rtc_month, rtc_day, rtc_hour, rtc_minute, rtc_second 设置为系统 RTC 时间 |
| get_rtc | 无 | 获取系统 RTC 时间，并设置到属性 rtc_year, rtc_month, rtc_day, rtc_hour, rtc_minute, rtc_second, rtc_wday |
| reset | 无 | 重置配置为默认值 |
| save | 无 | 保存配置 |
| reload | 无 | 重新加载持久化的配置
| beep_on | 时间长度(毫秒) | 打开蜂鸣器 |
| play_audio | 文件名 | 播放音频文件 |
| stop_audio | 无 | 停止播放音频文件 |
