 # AWTK 开源串口屏开发 - 用户和权限管理

用户管理和权限控制是一个常用的功能。在工业软件中，通常将用户分为几种不同的角色，每种角色有不同的权限，比如管理员、操作员和维护员等等。在 AWTK 串口屏中，内置基本的用户管理和权限控制功能，可以满足常见的需求。开发者不需要编写代码，设计好用户界面，通过数据和命令绑定规则，即可实现用户和权限管理功能，比如登录、登出、修改密码、权限控制、创建用户、删除用户等功能。

![](images/user_manager.gif)

本文介绍一下 AWTK 串口屏中的用户管理和权限控制功能。

## 1. 出厂默认用户

在下面的数据文件中，可以修改出厂默认的用户和密码。密码使用 sha256 加密，可以使用 [在线工具](https://emn178.github.io/online-tools/sha256.html) 生成。

```
design/default/data/user_manager.csv
```

比如 demo 中的默认用户是：

```
admin|0|8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918|管理员（默认密码：admin)
```

## 2. 数据文件格式

用 CSV 格式的文件存储用户名和密码，每行一个用户，每行的格式如下：

* 用户名。可以使用中文。
* 角色。用来做实际的权限控制，怎么定义就怎么用。比如：0 表示管理员，1 表示操作员，2 表示维护员。
* 密码。使用 sha256 加密。
* 备注。可以使用中文。

> 在后面的数据绑定中，name 表示用户名，role 表示角色，password 表示密码，memo 表示备注。

## 3. 用户登录

### 3.1 用户登录命令

当启用用户管理时，默认模型 (default) 会提供一个 login 的命令。

login 命令依赖两个属性：

* login_username 登录的用户名。
* login_password 登录的密码。

login 命令需要一个参数，用来指定登录成功后跳转的目标页面。

### 3.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {login_username} | 用户名编辑器 |
| v-data:value| {login_password, Mode=OneWayToModel, ToModel=sha256(value)} | 密码编辑器 |

* Mode=OneWayToModel 表示单向绑定，只能从控件到模型，主要是因为模型里的密码是加密后的哈希值，不能反向绑定到控件。
* 表示将控件输入的数据使用 sha256 加密后，再赋值给模型。

### 3.3 命令绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {login, Args=home_page} | 登录按钮的点击事件。home_page 是登录成功后跳转的页面 |

### 3.4 demo 参考界面

![](images/login.png)

## 4. 用户登录状态

### 4.1 用户登录状态

用户登录成功后，会在默认模型 (default) 中提供两个变量：

* username 登录的用户名。

* userrole 登录的用户角色。

userrole 是一个整数，可以通过 userrole 来控制权限。

### 4.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {username} | 显示当前用名 |
| v-data:value| {one_of('管理员；工程师；操作员', userrole)} | 显示当前的角色名 |

### 4.3 命令绑定

比如，只用管理员才能访问的页面，可以通过下面的命令绑定来实现：

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {navigate, Args=user_manager, AutoDisable=false} | AutoDisable 一定要设置为 false，否则不能绑定 enable 属性 |
| v-data:enable | {userrole==0} | 当前角色为管理员时，才启用本按钮 |

比如，只用管理员执行某些命令，可以通过下面的命令绑定来实现：

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {do_something, Args=xxx, AutoDisable=false} | AutoDisable 一定要设置为 false，否则不能绑定 enable 属性 |
| v-data:enable | {userrole==0} | 当前角色为管理员时，才启用本按钮 |

### 4.4 demo 参考界面

* 管理员界面，全部功能可用。

![](images/user_manager_admin.png)

* 工程师界面，部分功能可用。

![](images/user_manager_engineer.png)

## 5. 修改密码

### 5.1 修改密码命令

当启用用户管理时，默认模型 (default) 会提供一个 change_password 的命令。

change_password 命令依赖两个属性：

* change_password 修改的密码。
* change_confirm_password 确认修改的密码。

change_password 命令不需要参数。

### 5.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {change_password, Mode=OneWayToModel, ToModel=sha256(value)} | 密码编辑器 |
| v-data:value| {change_confirm_password, Mode=OneWayToModel, ToModel=sha256(value)} | 确认密码编辑器 |

### 5.3 命令绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {change_password, CloseWindow=true} | 确认按钮的点击事件。 |

### 5.4 demo 参考界面

![](images/change_password.png)

## 6. 用户管理

前面的模型都是默认模型 (default)，用户管理模型是 user_manager。

### 6.1 用户管理命令

当启用用户管理时，用户管理模型 (user_manager) 会提供一些命令。

* add 添加用户。
* remove 删除用户。
* edit 编辑用户。
* save 保存用户。
* reload 重新加载用户。
* set_selected 设置选中的用户。

### 6.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {selected_index} | 选中的用户索引，在列表项目外使用。 |
| v-data:value| {index} | 序数，在列表项内使用。|
| v-data:value| {item.name} | 用户名，在列表项内使用。|
| v-data:value| {one_of('管理员；工程师；操作员', item.role)} | 角色，在列表项内使用。 |
| v-data:value| {item.memo} | 备注，在列表项内使用。|

### 6.3 命令绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {add} | 添加 按钮的点击事件。 |
| v-on:click | {edit} | 编辑 按钮的点击事件。 |
| v-on:click | {save} | 保存 按钮的点击事件。 |
| v-on:click | {reload} | 重新加载 按钮的点击事件。 |
| v-on:click | {set_selected} | 列表项的点击事件。 |
| v-on:click | {remove, Args=selected_index, AutoDisable=false} | 删除按钮的点击事件。AutoDisable 一定要设置为 false，否则不能绑定 enable 属性 |

为了不让用户删除管理员用户，可以设置删除按钮的 enable 属性。绑定规则如下：

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:enable | {selected_index!=0} | 当前选中的用户不是管理员时，启用本按钮。 |

### 6.4 demo 参考界面

![](images/user_manager_main.png)

## 7. 创建用户

user\_manager 模型提供了一个 add 命令，用来创建用户。如果支持创建用户，需要提供一个创建用户的界面，窗口的名字必须是 user\_manager\_add。

通过 new 参数可以设置默认数据。比如：

```
user_manager(new=' |1| | |');
```

### 7.1 模型

创建用户的界面有自己的模型，代表当前创建的用户。

它具有下面的属性：

* name 表示 username 用户名。
* role 表示 role 角色。
* password 表示 password 密码。
* memo 表示 memo 备注。

它还提供了一个确认增加的命令。

* add 命令。

### 7.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {name, validator=username} | 用户名编辑器，内置数据校验器 username，用于检查用户名的有效性（如是否重名） |
| v-data:value| {role} | 角色编辑器 |
| v-data:value| {password, Mode=OneWayToModel, ToModel=sha256(value)} | 密码编辑器 |
| v-data:value| {memo} | 备注编辑器 |

### 7.3 命令绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {add, CloseWindow=true} | 确认按钮的点击事件。 |

### 7.4 demo 参考界面

![](images/user_manager_add.png)

## 8. 编辑用户

user\_manager 模型提供了一个 edit 命令，用来编辑用户。如果支持编辑用户，需要提供一个编辑用户的界面，窗口的名字必须是 user\_manager\_edit。

### 8.1 模型

编辑用户的界面有自己的模型，代表当前编辑的用户。

它具有下面的属性：

* name 表示 username 用户名。
* role 表示 role 角色。
* memo 表示 memo 备注。

不需要额外的命令。

### 8.2 数据绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {name} | 用户名编辑器 |
| v-data:value| {role, Trigger=Explicit} | 角色编辑器，为了方便取消，采用显式更新 |
| v-data:value| {memo, Trigger=Explicit} | 备注编辑器，为了方便取消，采用显式更新  |

### 8.3 命令绑定

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click | {nothing, UpdateModel=true, CloseWindow=true} | 确认按钮的点击事件。 |
| v-on:click | {nothing, CloseWindow=true} | 取消按钮的点击事件。 |

> UpdateModel=true 表示更新模型，CloseWindow=true 表示关闭窗口。

### 8.4 demo 参考界面

![](images/user_manager_edit.png)

## 9. 注意

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。

* 实际开发时，可以参考 demo_user_manager 演示项目，在的它 UI 文件上修改。
