# AWTK 开源串口屏开发 - 系统设置

系统设置只是一个普通应用程序，不过它会用 [默认模型](default_model.md) 中一些内置的属性和命令，所以这里专门来介绍一下。

## 1. 功能

在这个例子会用到 [默认模型](default_model.md) 中一些下列内置的属性和命令：

* 内置属性

| 属性 | 类型 | 说明 |
| :-----| ---- | :---- |
| rtc_year | int | RTC 年 |
| rtc_month | int | RTC 月 |
| rtc_day | int | RTC 日 |
| rtc_hour | int | RTC 时 |
| rtc_minute | int | RTC 分 |
| rtc_second | int | RTC 秒 |
| rtc_ymd | string | RTC 年月日，格式为 YYYY-MM-DD 或 YYYY/MM/DD |
| rtc_hms | string | RTC 时分秒，格式为 HH:MM:SS |
| backlight | int | 背光亮度 (0-100) |
| audio_volume | int | 音量 (0-100) |
| ui_feedback | bool | UI 反馈。true 启用反馈 (beep), false 禁用反馈 |

* 内置命令

| 命令 | 参数 | 说明 |
| :-----| ---- | :---- |
| set_rtc | 无 | 将属性 rtc_year, rtc_month, rtc_day, rtc_hour, rtc_minute, rtc_second 设置为系统 RTC 时间 |
| get_rtc | 无 | 获取系统 RTC 时间，并设置到属性 rtc_year, rtc_month, rtc_day, rtc_hour, rtc_minute, rtc_second, rtc_wday |
| save | 无 | 保存配置 |

## 2. 创建项目

从模板创建项目，将 hmi/template_app 拷贝 hmi/settings 即可。

> 最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

用 AWStudio 打开上面 settings 目录下的 project.json 文件。

里面有一个空的窗口，在上面加入下面的控件，并调节位置和大小，做出类似下面的界面。

![](images/settings0.png)

再创建一个新窗口，命名为 basic，并加入下面的控件，并调节位置和大小，做出类似下面的界面。

![](images/settings1.png)

再创建一个新窗口，命名为 rtc，并加入下面的控件，并调节位置和大小，做出类似下面的界面。

![](images/settings2.png)

> 需要给编辑器设置输入类型、最大值和最小值。实际项目可能会用**文本选择**控件代替**编辑器**，不过绑定方法是一样的。

## 3. 添加绑定规则

## 3.1 主界面 (home_page)

* 将 **基本设置** 按钮的 **点击** 事件绑定到 **navigate** 命令。添加自定义的属性 **v-on:click**，将值设置为 **{navigate, Args=basic}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {navigate, Args=basic} | navigate 命令是导航命令，用于打开窗口，Args=后面跟目标窗口的名称。|

* 将 **时间设置** 按钮的 **点击** 事件绑定到 **navigate** 命令。添加自定义的属性 **v-on:click**，将值设置为 **{navigate, Args=rtc}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {navigate, Args=rtc} | navigate 命令是导航命令，用于打开窗口，Args=后面跟目标窗口的名称。|

> 命令和参数要用英文大括号括起来。

## 3.2 基本设置 (basic)

* 将 **背光 滑动条** 绑定到 **backlight** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{backlight}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {backlight} | 变量要用英文大括号括起来。 |

* 将 **音量 滑动条** 绑定到 **audio_volume** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{audio_volume}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {audio_volume} | 变量要用英文大括号括起来。 |

* 将 **开启屏幕音** 绑定到 **ui_feedback** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{ui_feedback}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {ui_feedback} | 变量要用英文大括号括起来。 |

* 让 **确定** 按钮的 **点击** 事件执行命令 "{save}"。添加自定义的属性 **v-on:click**，将值设置为：

```
{save, CloseWindow=true}
```

> CloseWindow=true，表示执行命令并关闭当前窗口。

> 命令和参数要用英文大括号括起来。

* 同样指定窗口的模型为 default。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model| default | default 不需要用大括号括起来。 |

## 3.3 时间设置 (rtc)

* 将 **年 编辑器** 绑定到 **rtc_year** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_year}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_year} | 变量要用英文大括号括起来。 |

* 将 **月 编辑器** 绑定到 **rtc_month** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_month}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_month} | 变量要用英文大括号括起来。 |

* 将 **日 编辑器** 绑定到 **rtc_day** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_day}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_day} | 变量要用英文大括号括起来。 |

* 将 **时 编辑器** 绑定到 **rtc_hour** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_hour}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_hour} | 变量要用英文大括号括起来。 |

* 将 **时 编辑器** 绑定到 **rtc_minute** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_minute}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_minute} | 变量要用英文大括号括起来。 |

* 将 **秒 编辑器** 绑定到 **rtc_second** 变量。添加自定义的属性 **v-data:value**，将值设置为 **{rtc_second}**。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {rtc_second} | 变量要用英文大括号括起来。 |

* 让 **获取时间** 按钮的 **点击** 事件执行命令 "{get_rtc}"。添加自定义的属性 **v-on:click**，将值设置为：

```
{get_rtc}
```

> 命令和参数要用英文大括号括起来。

* 让 **设置时间** 按钮的 **点击** 事件执行命令 "{set_rtc}"。添加自定义的属性 **v-on:click**，将值设置为：

```
{set_rtc}
```

> 命令和参数要用英文大括号括起来。

* 让 **返回** 按钮的 **点击** 事件执行命令 "{nothing}"。添加自定义的属性 **v-on:click**，将值设置为：

```
{nothing, CloseWindow=true}
```

> nothing 是一个特殊命令，表示什么也不做。
 
> CloseWindow=true，表示执行命令并关闭当前窗口。

> 命令和参数要用英文大括号括起来。

* 同样指定窗口的模型为 default。

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model| default | default 不需要用大括号括起来。 |

## 4. 初始化数据

修改资源文件 design/default/data/default_model.json, 将其内容改为：

```json
{
  "ui_feedback":true,
  "audio_volume":60,
  "backlight":60
}
```

注意：

* 如果文件内容有中文（非 ASCII 字符），一定要保存为 UTF-8 格式。

* 重新打包资源才能生效。

## 5. 描述需要持久化的数据

修改资源文件 design/default/data/settings.json, 将其内容改为：

```json
{
    "name": "hmi_settings",
    "persistent": {
        "ui_feedback": true,
        "audio_volume": true,
        "backlight": true
    }
}
```

> 如果有多个数据需要持久化，将它们加入到 settings 对象中即可。为了避免不同串口屏应用程序之间，持久化数据覆盖，这里的 **name** 需要取一个唯一的名字。

注意：

* 如果文件内容有中文（非 ASCII 字符），一定要保存为 UTF-8 格式。

* 重新打包资源才能生效。

## 5. 编译运行 

运行 bin 目录下的 demo 程序。

![](images/settings.gif)

> 程序退出时会自动保存需要保存的数据。
 
## 7. 注意

本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。
