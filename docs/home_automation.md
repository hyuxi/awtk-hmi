# AWTK 串口屏 智能家居示例

## 1. 功能

这个例子稍微复杂一点，界面这里直接使用了 [立功科技 ZDP1440 HMI 显示驱动芯片](https://zlgmcu.com/intelligentchip/intelligentchip/product/id/231.html) 例子中的 UI 文件和资源，重点关注数据绑定。在这里例子中，模型（也就是数据）里包括一台空调和一台咖啡机：

| 变量名 | 数据类型 | 功能说明 |
| :-----| ---- | :---- |
|空调_开关 | 布尔 | 空调开关|
|空调_模式 | 整数 | 空调模式 (0: 制冷；1: 制热；2: 送风；3: 除湿；4: 自动）|
|空调_风速 | 整数 | 0-4 共五档|
|空调_垂直风向 | 整数 | 垂直风向 (0: 自动；1：上；2：中；3：下）|
|空调_水平风向 | 整数 | 水平风向（0：自动；1：左；2：中；3：右）|
|空调_温度 | 整数 | 温度（0-40） |
|咖啡_开关 | 布尔 | 咖啡开关 |
|咖啡_类型 | 整数 | 类型 (0: 卡布奇诺；1: 拿铁；2: 美式；3: 意式）|
|咖啡_温度 | 整数 | 温度（0-100）|
|咖啡_口味 | 整数 | 口味 (0: 浓郁；1: 丝滑；2: 清淡；3: 平衡；4: 温和）|
|咖啡_热奶 | 整数 | 热奶 (0: 少量；1: 较少；2: 较多；3: 大量） |
|咖啡_奶泡 | 整数 | 奶泡 (0: 少量；1: 较少；2: 较多；3: 大量）|
|咖啡_水量 | 整数 | 水量 (50-350ml)|
|咖啡_剩余时间 | 整数 | 制作时间（格式：分钟：秒）|
|咖啡_开始制作 | 布尔 | 开始制作|

## 2. 创建项目

从模板创建项目，将 hmi/template_app 拷贝 hmi/home_automation 即可。

> 项目最好不要放到其它目录，因为放到其它目录需要修改配置文件中的路径，等熟悉之后再考虑放到其它目录。路径中也不要中文和空格，避免不必要的麻烦。

## 3. 制作界面

界面和资源就直接用 [立功科技 ZDP1440 HMI 显示驱动芯片](https://zlgmcu.com/intelligentchip/intelligentchip/product/id/231.html) 例子了：

* 主界面

![](images/home_automation0.png)

* 空调界面

![](images/home_automation1.png)

* 咖啡机界面

![](images/home_automation2.png)

## 3. 添加绑定规则

里面的控件太多，为了不至于太累赘，不同类型的绑定只举一个例子：

> 完整示例可以参考 hmi/demo_home2

### 3.1 温度设置

这种两个按钮带一个静态文本的组合很常见。

![](images/home_automation3.png)

* 中间的静态文本

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {空调_温度+'℃'} ||

* 右边的按钮（增加）

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=set(空调_温度，min(空调_温度+1, 40))} | 这里用函数 [set](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#418-set) 将变量**空调_温度**增加 1 度。[min](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#456-min) 函数保证变量的值不会超出 40。|

* 左边的按钮（减少）

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-on:click| {fscript, Args=set(空调_温度，max(空调_温度-1, 40))} | 这里用函数 [set](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#418-set) 将变量**空调_温度**减少 1 度。[max](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#456-max) 函数保证变量的值不会小于 0。|

> **v-on:click** 是一个常见的事件，最好记住，可以提高效率。

### 3.2 模式选择

这个用一组单选按钮实现，将多个单选按钮放到 group_box 里（将多个单选按钮放到 view 里也可以，只是需要为每个单选按钮编写绑定规则）。

![](images/home_automation4.png)

* 如果绑定规则写到 group_box 上。这样写即可：

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {空调_模式} | |

* 如果使用 view 作为容器，则麻烦一点。需要为每个单选按钮编写两条绑定规则：

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| {空调_模式 == 0} | 这里的 0 是单选按钮的序数，根据实际情况调整 |
| v-on:click | {fscript, Args=set(空调_模式，0)} | 这里的 0 是单选按钮的序数，根据实际情况调整 |

> 这种方式虽然麻烦，但是可以处理变量的值不是从 0 开始或者不连续的情况。

### 3.3 模式显示

**模式**用一个静态文本显示。问题在于，**模式**在内部用一个正整数表示，而显示的是一个用户可以理解的字符串。所以需要一个转换函数 [one_of](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#422-one_of)：

![](images/home_automation5.png)

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| v-data:value="{one_of('制冷;制热;送风;除湿;自动', 空调_模式)} | 这里的 [one_of](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#422-one_of) 的功能是从指定的字符串数组中取出对应的子串。|


### 3.4 剩余时间

**剩余时间**用一个静态文本显示。问题在于，**剩余时间**在内部用一个正整数表示(秒数)，而显示的是“分钟:秒”。所以需要一个转换表达式：

![](images/home_automation6.png)

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:value| int(咖啡_剩余时间/60) + ':' + 咖啡_剩余时间%60} | 表达是按浮点数计算的，这里的 [int](https://gitee.com/zlgopen/awtk/blob/master/docs/fscript.md#4110-int) 将结果转换为正数。|

### 5.5 隐藏视图

在点击开关按钮时，会自动显示或隐藏右边的设置视图。这是通过将视图的可见性(visible)绑定到开关的状态实现的:

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-data:visible | {空调_开关} | |


### 5.6 指定窗口的模型为 default

这是最简单也是最关键的一步:

| 绑定属性 | 绑定规则 | 说明 |
| :-----| ---- | :---- |
| v-model | default | |

> 严格的意义上说，绑定规则也是一种代码，不过相比于 C 语言，它有下面的优势：

* 无需编译，直接运行
* 简单，通常只有一行。
* 易懂，声明式的语法。

## 4. 初始化数据

修改资源文件 design/default/data/default_model.json, 将其内容改为：

```json
{
  "空调_开关" : false,
  "空调_模式" : 3,
  "空调_风速" : 3,
  "空调_垂直风向" : 1,
  "空调_水平风向" : 1,
  "空调_温度": 25,
  "咖啡_开关" : false,
  "咖啡_类型" : 1,
  "咖啡_温度" : 60,
  "咖啡_口味" : 1,
  "咖啡_热奶" : 1,
  "咖啡_奶泡" : 1,
  "咖啡_水量" : 150,
  "咖啡_剩余时间" : 200,
  "咖啡_开始制作" : false
}
```

注意：

* 如果文件内容有中文(非ASCII字符)，一定要保存为 UTF-8 格式。

* 重新打包资源才能生效。

## 5. 编译运行 

运行 bin 目录下的 demo 程序。

![](images/home_automation7.png)

## 6. 使用 MCU 模拟器与之进行交互

运行 mcu/simulator 目录下的 mcu_sim 程序，连接到 Localhost:2233。

* 在界面上修改参数，会看到模拟器上收到了对应的事件：

![](images/home_automation1.gif)

* 在模拟器中设置变量 **咖啡_类型** 的数据，HMI 端的界面也会自动更新。

![](images/home_automation2.gif)

## 7. 注意

* 完整示例请参考：demo_home2

* 本项目并没有编写界面相关的代码，AWStudio 在 src/pages 目录下生成了一些代码框架，这些代码并没有用到，可以删除也可以不用管它，但是不能加入编译。

* 本项目中的界面和资源来自 [立功科技 ZDP1440 HMI 显示驱动芯片](https://zlgmcu.com/intelligentchip/intelligentchip/product/id/231.html) 例子，仅用于演示，不可用于商业用途。
