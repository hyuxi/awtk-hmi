# MCU 端开发环境搭建

AWTK 开源串口屏 MCU 端默认采用开发板 [Easy-HC32F460](https://www.zlgmcu.com/utilitymcu/utilitymcu/product/id/5.html)，本文介绍一下搭建开发环境和移植 HMI 客户端的过程。

## 1. 下载 Ametal

```sh
git clone https://gitee.com/zlgmcuopen/ametal.git
```

## 2. Keil 5 安装 相应的软件包

位置在：

```
ametal\tools\keil_pack\HDSC.HC32F46x.1.0.2.pack
```

## 3. 用 Keil 5 打开示例工程

位置在：

```
ametal\board\hc32f460_core\project_example\projects_keil5\example_hc32f460_core.uvprojx
```

## 4. 硬件连接

* SWD 连接到 J3 几个 PIN 上。

![](images/hc32_f460_swd.png)

* UART 示例默认用的 UART4: PB3 为 TX 引脚，PB4 为 RX 引脚。

![](images/hc32_f460_uart.png)

## 5. Keil 调试器设置

调试器可以参考下面的设置“

![](images/hc32_f460_debug1.png)

![](images/hc32_f460_debug2.png)

## 6. 加入 mini-sdk 中的文件

将 hmi.c/hmi.h/types_def.h 等文件放如下面的目录：

```
ametal\board\hc32f460_core\project_example\user_code
```

将hmi.c文件加入工程：

![](images/hc32_f460_hmi.png)

## 7. 示例参考代码


```c
#include "hmi.h"
#include "ametal.h"
#include "am_uart.h"
#include "am_uart_rngbuf.h"
#include "am_delay.h"
#include "am_hc32f460_inst_init.h"

#define UART_RX_BUF_SIZE 128
#define UART_TX_BUF_SIZE 128

static uint8_t __uart_rxbuf[UART_RX_BUF_SIZE];
static uint8_t __uart_txbuf[UART_TX_BUF_SIZE];
static am_uart_rngbuf_dev_t __g_uart_ringbuf_dev;

static hmi_t hmi;
static int s_value = 0;
#define STR_PROP_TEMP "温度"

static int32_t hmi_uart_read(hmi_t *hmi, void *data, uint32_t size)
{
    am_uart_rngbuf_handle_t handle = (am_uart_rngbuf_handle_t)(hmi->ctx);

    return am_uart_rngbuf_receive(handle, data, size);
}

static int32_t hmi_uart_write(hmi_t *hmi, const void *data, uint32_t size)
{
    am_uart_rngbuf_handle_t handle = (am_uart_rngbuf_handle_t)(hmi->ctx);

    return am_uart_rngbuf_send(handle, data, size);
}

static ret_t hmi_uart_on_prop_changed(hmi_t *hmi, const char *name, const value_t *v)
{
    if (strcmp(name, STR_PROP_TEMP) == 0)
    {
        s_value = value_int32(v);
    }

    return RET_OK;
}

// the setup function runs once when you press reset or power the board
am_uart_rngbuf_handle_t hmi_setup(am_uart_handle_t uart_handle)
{
    am_uart_rngbuf_handle_t handle = NULL;

    handle = am_uart_rngbuf_init(&__g_uart_ringbuf_dev,
                                 uart_handle,
                                 __uart_rxbuf,
                                 UART_RX_BUF_SIZE,
                                 __uart_txbuf,
                                 UART_TX_BUF_SIZE);
    hmi_init(&hmi, hmi_uart_read, hmi_uart_write, hmi_uart_on_prop_changed, handle);
  
    return handle;
}

// the loop function runs over and over again forever
int hmi_loop(am_uart_rngbuf_handle_t handle)
{
    while (1)
    {
        if (am_rngbuf_nbytes(&(handle->rx_rngbuf)) > 10)
        {
            hmi_dispatch(&hmi);
        }
        else
        {
            s_value++;
            if (s_value > 100)
            {
                s_value = 0;
            }

            hmi_set_prop_int(&hmi, STR_PROP_TEMP, s_value);
            am_mdelay(100);
        }
    }

    return 0;
}

int hmi_ametal_demo(void) {
    return hmi_loop(hmi_setup(am_hc32f460_uart4_inst_init()));
}
```