# MCU SDK

完整示例可以参考下面的几个例子：

* 普通嵌入式系统 mcu/stm32/hmi\_app/hmi\_app.c

* 低端嵌入式系统 mcu/mini-sdk/hmi/examples/socket/main.c

* Arduino 系统 mcu/mini-sdk/hmi/examples/arduino/awtk\_hmi\_demo.ino

![](images/arduino.jpg)

* MCU 模拟器 simulator/src/pages/home_page.c

![](images/home_automation1.gif)

## 基本用法

* 创建 hmi 对象

创建 hmi 对象时，需要提供一个回调函数，当属性变化时，会调用这个函数。

示例

```c
static ret_t hmi_on_prop_change(hmi_t* hmi, const char* name, const value_t* v) {
  /*处理参数变化*/
  if (tk_str_eq(name, "温度")) {
    int32_t temp = value_int(v);
    log_debug("temp=%d\n", temp);
  }

  return RET_OK;
}

...
  io = tk_stream_factory_create_iostream(url);
  hmi = hmi_create(io, hmi_on_prop_change, NULL);
```

* 设置属性

示例

```c
  hmi_set_prop_int(hmi, "温度", 36);
```

* 获取属性

示例

```c
  int32_t temp = hmi_get_prop_int(hmi, "温度", 0);
```

* 在主循环中分发事件
  
示例

```c
  hmi_dispatch(hmi);
```
## 完整示例

```c

/*本文必须保存为 UTF-8 BOM 格式 */

#include "tkc/mem.h"
#include "hmi/hmi.h"

static int s_value = 0;
static uint32_t s_heap_mem[10240];

#define HMI_PROP_TEMP "温度"

/*回调函数*/
static ret_t hmi_on_prop_change(hmi_t* hmi, const char* name, const value_t* v) {
  if (strcmp(name, "温度") == 0) {
    s_value = value_int(v);
  }

  return RET_OK;
}

static void system_init(void) {
  Cache_Enable();                  //打开L1-Cache
  HAL_Init();                      //初始化HAL库
  Stm32_Clock_Init(160, 5, 2, 4);  //设置时钟,400Mhz
  delay_init(400);                 //延时初始化
  LED_Init();                      //初始化LED
  KEY_Init();                      //初始化按键
}

int main(void) {
  u8 key = 0;
  hmi_t* hmi = NULL;

  system_init();

  /*初始化内存*/
  tk_mem_init(s_heap_mem, sizeof(s_heap_mem));

  /*创建HMI对象*/
  hmi = hmi_create_with_serial("1", hmi_on_prop_change, NULL);

  while (1) {
    key = KEY_Scan(0);
    if (key) {
      switch (key) {
        case KEY2_PRES: {
          s_value = 0;
          break;
        }
        case KEY1_PRES: {
          s_value--;
          break;
        }
        case KEY0_PRES: {
          s_value++;
          break;
        }
        default: {
          break;
        }
      }

      /*修改数据*/
      hmi_set_prop_int(hmi, HMI_PROP_TEMP, s_value);
    } else {
      delay_ms(10);
    }

    /*分发事件*/
    hmi_dispatch(hmi);
  }
}
```

## API 参考

串口屏客户端(供 MCU 使用)。

----------------------------------
### 函数
<p id="hmi_t_methods">

| 函数名称 | 说明 | 
| -------- | ------------ | 
| <a href="#hmi_t_hmi_create">hmi\_create</a> | 创建hmi对象。 |
| <a href="#hmi_t_hmi_create_with_serial">hmi\_create\_with\_serial</a> | 创建hmi对象。 |
| <a href="#hmi_t_hmi_destroy">hmi\_destroy</a> | 销毁hmi对象。 |
| <a href="#hmi_t_hmi_dispatch">hmi\_dispatch</a> | 处理事件。 |
| <a href="#hmi_t_hmi_get_prop">hmi\_get\_prop</a> | 获取属性。 |
| <a href="#hmi_t_hmi_get_prop_bool">hmi\_get\_prop\_bool</a> | 获取布尔属性。 |
| <a href="#hmi_t_hmi_get_prop_float">hmi\_get\_prop\_float</a> | 获取浮点数属性。 |
| <a href="#hmi_t_hmi_get_prop_int">hmi\_get\_prop\_int</a> | 获取整数属性。 |
| <a href="#hmi_t_hmi_get_prop_int64">hmi\_get\_prop\_int64</a> | 获取64位整数属性。 |
| <a href="#hmi_t_hmi_get_prop_str">hmi\_get\_prop\_str</a> | 获取字符串属性。 |
| <a href="#hmi_t_hmi_set_prop">hmi\_set\_prop</a> | 设置属性。 |
| <a href="#hmi_t_hmi_set_prop_bool">hmi\_set\_prop\_bool</a> | 设置布尔属性。 |
| <a href="#hmi_t_hmi_set_prop_float">hmi\_set\_prop\_float</a> | 设置浮点数属性。 |
| <a href="#hmi_t_hmi_set_prop_int">hmi\_set\_prop\_int</a> | 设置整数属性。 |
| <a href="#hmi_t_hmi_set_prop_int64">hmi\_set\_prop\_int64</a> | 设置64位整数属性。 |
| <a href="#hmi_t_hmi_set_prop_str">hmi\_set\_prop\_str</a> | 设置字符串属性。 |
### 属性
<p id="hmi_t_properties">

| 属性名称 | 类型 | 说明 | 
| -------- | ----- | ------------ | 
| <a href="#hmi_t_remote_ui">remote\_ui</a> | remote\_ui\_t* | remote ui 对象。 |
#### hmi\_create 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_create">创建hmi对象。

* 函数原型：

```
hmi_t* hmi_create (tk_iostream_t* io, hmi_on_prop_changed_t on_prop_changed, void* ctx);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | hmi\_t* | 返回hmi对象。 |
| io | tk\_iostream\_t* | 流对象。 |
| on\_prop\_changed | hmi\_on\_prop\_changed\_t | 属性变化回调函数。 |
| ctx | void* | 上下文。 |
#### hmi\_create\_with\_serial 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_create_with_serial">创建hmi对象。

* 函数原型：

```
hmi_t* hmi_create_with_serial (const char* device, hmi_on_prop_changed_t on_prop_changed, void* ctx);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | hmi\_t* | 返回hmi对象。 |
| device | const char* | 串口设备。 |
| on\_prop\_changed | hmi\_on\_prop\_changed\_t | 属性变化回调函数。 |
| ctx | void* | 上下文。 |
#### hmi\_destroy 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_destroy">销毁hmi对象。

* 函数原型：

```
ret_t hmi_destroy (hmi_t* hmi);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
#### hmi\_dispatch 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_dispatch">处理事件。

* 函数原型：

```
ret_t hmi_dispatch (hmi_t* hmi);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
#### hmi\_get\_prop 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop">获取属性。

* 函数原型：

```
ret_t hmi_get_prop (hmi_t* hmi, const char* target, const char* name, value_t* v);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| target | const char* | 目标对象。 |
| name | const char* | 属性名称。 |
| v | value\_t* | 属性值。 |
#### hmi\_get\_prop\_bool 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop_bool">获取布尔属性。

* 函数原型：

```
bool_t hmi_get_prop_bool (hmi_t* hmi, const char* name, bool_t defvalue);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | bool\_t | 返回属性值。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| defvalue | bool\_t | 默认值。 |
#### hmi\_get\_prop\_float 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop_float">获取浮点数属性。

* 函数原型：

```
float hmi_get_prop_float (hmi_t* hmi, const char* name, float defvalue);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | float | 返回属性值。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| defvalue | float | 默认值。 |
#### hmi\_get\_prop\_int 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop_int">获取整数属性。

* 函数原型：

```
int32_t hmi_get_prop_int (hmi_t* hmi, const char* name, int32_t defvalue);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | int32\_t | 返回属性值。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| defvalue | int32\_t | 默认值。 |
#### hmi\_get\_prop\_int64 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop_int64">获取64位整数属性。

* 函数原型：

```
int64_t hmi_get_prop_int64 (hmi_t* hmi, const char* name, int64_t defvalue);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | int64\_t | 返回属性值。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| defvalue | int64\_t | 默认值。 |
#### hmi\_get\_prop\_str 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_get_prop_str">获取字符串属性。

* 函数原型：

```
const char* hmi_get_prop_str (hmi_t* hmi, const char* name, const char* defvalue);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | const char* | 返回属性值。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| defvalue | const char* | 默认值。 |
#### hmi\_set\_prop 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop">设置属性。

* 函数原型：

```
ret_t hmi_set_prop (hmi_t* hmi, const char* target, const char* name, const value_t* v);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| target | const char* | 目标对象。 |
| name | const char* | 属性名称。 |
| v | const value\_t* | 属性值。 |
#### hmi\_set\_prop\_bool 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop_bool">设置布尔属性。

* 函数原型：

```
ret_t hmi_set_prop_bool (hmi_t* hmi, const char* name, bool_t value);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| value | bool\_t | 属性值。 |
#### hmi\_set\_prop\_float 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop_float">设置浮点数属性。

* 函数原型：

```
ret_t hmi_set_prop_float (hmi_t* hmi, const char* name, float value);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| value | float | 属性值。 |
#### hmi\_set\_prop\_int 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop_int">设置整数属性。

* 函数原型：

```
ret_t hmi_set_prop_int (hmi_t* hmi, const char* name, int32_t value);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| value | int32\_t | 属性值。 |
#### hmi\_set\_prop\_int64 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop_int64">设置64位整数属性。

* 函数原型：

```
ret_t hmi_set_prop_int64 (hmi_t* hmi, const char* name, int64_t value);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| value | int64\_t | 属性值。 |
#### hmi\_set\_prop\_str 函数
-----------------------

* 函数功能：

> <p id="hmi_t_hmi_set_prop_str">设置字符串属性。

* 函数原型：

```
ret_t hmi_set_prop_str (hmi_t* hmi, const char* name, const char* value);
```

* 参数说明：

| 参数 | 类型 | 说明 |
| -------- | ----- | --------- |
| 返回值 | ret\_t | 返回RET\_OK表示成功，否则表示失败。 |
| hmi | hmi\_t* | hmi对象。 |
| name | const char* | 属性名称。 |
| value | const char* | 属性值。 |
#### remote\_ui 属性
-----------------------
> <p id="hmi_t_remote_ui">remote ui 对象。
> 高级用户可以使用此对象直接操作远程UI。

* 类型：remote\_ui\_t*

| 特性 | 是否支持 |
| -------- | ----- |
| 可直接读取 | 是 |
| 可直接修改 | 否 |
