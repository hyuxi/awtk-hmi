# HMI_ZDP1440D 开发环境搭建

## 安装相关工具（如果已经安装请跳过）

* [git windows](https://gitforwindows.org)
* [微软 VSCode](https://code.visualstudio.com/)
* [AWTK Designer](https://awtk.zlg.cn/awstudio/download.html)

> 安装 AWTK Designer 后，请根据要求安装 AWTK 编译环境。

## 下载 AWTK/AWTK-MVVM 源码和交叉编译环境

* 下载交叉编译环境

```sh
git clone https://gitee.com/zlgopen/awtk-scons-cross-compile.git
```

* 下载并交叉编译 AWTK/AWTK-MVVM

进入 awtk-scons-cross-compile 目录，双击执行 prepare.bat 批处理命令，等待完成。

## 下载 AWTK-HMI

* 下载

```sh
git clone https://gitee.com/zlgopen/awtk-hmi.git 
```

说明：

* 确保 awtk-hmi 和 awtk-scons-cross-compile 并列在同一个目录

* 下载并编译 AWTK/AWTK-MVVM

进入 awtk-hmi 目录，执行 prepare.bat 批处理命令，等待完成。

## 编译 demo

demos 在 hmi 目录，后面以 demo_home2 为例。

### 编译 PC 版，并模拟运行

使用 AWTK Designer 打开 demo_home2 目录下的 project.json。然后：
![](images/designer_open_demo_home2.png)

* 打包
* 编译
* 模拟运行

![](images/designer_demo_home2_main.png)

### 交叉编译 HMI_ZDP1440D 版，并在开发版上运行

* 编译

> 进入 demo_home2 目录，双击执行 cross_build.bat 批处理命令。

成功执行后，会自动生成文件系统文件：ui_nor.bin

* 通过 T 卡升级

1. 在电脑端插上 T 卡，将前面生成的文件 ui_nor.bin 拷贝到 T 卡根目录，并弹出 T 卡。

> 如果 T 卡根目录下存在文件 ui_nor.bin，执行 cross_build.bat 时，会自动将生成的 ui_nor.bin 更新到 T 卡。

2. 将 T 卡插入 HMI_ZDP1440D 开发板的卡槽。同时按下 key 和 rst 两个键，系统重启，进入 UI 升级界面后，再松开，等待升级完成。

## 参考

* [HMI_ZDP1440D 官网](https://gitee.com/zlgmcuopen/HMI_ZDP1440D)

 * [AWTK 开发及调试环境搭建](https://z.zlg.cn/articleinfo?id=853202)