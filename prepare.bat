@echo off

git pull

IF EXIST awtk (
  echo "awtk exist."
  cd awtk
  git pull
  cd ..
) ELSE (
  git clone https://gitee.com/zlgopen/awtk.git 
)


IF EXIST awtk-mvvm (
  echo "awtk-mvvm exist."
  cd awtk-mvvm
  git pull
  cd ..
) ELSE (
  git clone https://gitee.com/zlgopen/awtk-mvvm.git 
)

cd awtk
scons
cd ..

cd awtk-mvvm
scons WITH_JERRYSCRIPT=False
cd ..

pause
