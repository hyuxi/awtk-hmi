git pull

if [ -e awtk ]
then
  echo "awtk exist."
  cd awtk
  git pull
  cd ..
else  
  git clone https://gitee.com/zlgopen/awtk.git 
fi

if [ -e awtk-mvvm ]
then
  echo "awtk-mvvm exist."
  cd awtk-mvvm
  git pull
  cd ..
else  
  git clone https://gitee.com/zlgopen/awtk-mvvm.git 
fi

cd awtk
scons
cd ..

cd awtk-mvvm
scons WITH_JERRYSCRIPT=False
cd ..

