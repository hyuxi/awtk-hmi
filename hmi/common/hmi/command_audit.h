/**
 * File:   command_audit.h
 * Author: AWTK Develop Team
 * Brief:  command_audit
 *
 * Copyright (c) 2023 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-12-18 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#ifndef TK_COMMAND_AUDIT_H
#define TK_COMMAND_AUDIT_H

#include "tkc/object.h"
#include "conf_io/conf_node.h"

BEGIN_C_DECLS

/**
 * @class command_audit_t
 * @annotation ["fake"]
 * 记录命令执行日志。
 * |时间|用户|命令|参数 
 */

/**
 * @method command_audit_init
 * 初始化command_audit。
 *
 * @param {tk_object_t*} model 默认模型对象。
 * @param {conf_doc_t*} settings 配置信息。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t command_audit_init(tk_object_t* model, conf_doc_t* settings);

/**
 * @method command_audit_deinit
 * 销毁command_audit。
 *
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t command_audit_deinit(void);

/**
 * @method command_audit_register
 * 注册command_audit。
 * @param {tk_object_t*} model 模型对象。
 * @return {ret_t} 返回RET_OK表示成功，否则表示失败。
 */
ret_t command_audit_register(tk_object_t* model);

#define COMMAND_AUDIT_PROP_NAME "command_audit"

#define COMMAND_AUDIT_PROP_QUERY_LEVEL CSV_QUERY_PREFIX"level"
#define COMMAND_AUDIT_PROP_QUERY_START_DATE CSV_QUERY_PREFIX"start_date"
#define COMMAND_AUDIT_PROP_QUERY_START_TIME CSV_QUERY_PREFIX"start_time"
#define COMMAND_AUDIT_PROP_QUERY_END_DATE CSV_QUERY_PREFIX"end_date"
#define COMMAND_AUDIT_PROP_QUERY_END_TIME CSV_QUERY_PREFIX"end_time"
#define COMMAND_AUDIT_PROP_QUERY_KEYWORDS CSV_QUERY_PREFIX"keywords"
#define COMMAND_AUDIT_PROP_QUERY_DEVICE CSV_QUERY_PREFIX"device"

#define COMMAND_AUDIT_FIELD_TIME "time"
#define COMMAND_AUDIT_FIELD_USERNAME "username"
#define COMMAND_AUDIT_FIELD_COMMAND "command"
#define COMMAND_AUDIT_FIELD_ARGS "args"

END_C_DECLS

#endif /*TK_COMMAND_AUDIT_H*/
