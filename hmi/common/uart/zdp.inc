
#include "base/window_manager.h"
#include "awtk_func.h"

typedef struct _uart_device_t {
  ring_buffer_t* rx_ring_buff;
} uart_device_t;


#define MAX_UART_COUNT 4
static uart_device_t s_uart_devices[MAX_UART_COUNT];

static ret_t deal_uart_data(void* ctx, event_t* e) {
  str_t uart_data;
  uart_receive_event_t* rx_e = uart_receive_event_cast(e);
  uart_device_t* dev = (uart_device_t*)ctx;

  ring_buffer_write(dev->rx_ring_buff, rx_e->data, rx_e->data_len);

  return RET_OK;
}

static ret_t uart_do_open(uart_device_t* dev, int fd, int baudrate) {
  widget_on(window_manager(), EVT_USER_RX_DATA, deal_uart_data, dev);

  return RET_OK;
}

static ret_t uart_do_close(uart_device_t* dev) {
  return RET_OK;
}

static int uart_do_send(uart_device_t* dev, const void* data, uint32_t size) {
  return awtk_data_send(data, size);
}