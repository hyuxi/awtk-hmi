﻿/**
 * File:   hmi_hal_dummy.c
 * Author: AWTK Develop Team
 * Brief:  hmi hal dummy
 *
 * Copyright (c) 2019 - 2023  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * License file for more details.
 *
 */

/**
 * History:
 * ================================================================
 * 2023-12-2 Li XianJing <xianjimli@hotmail.com> created
 *
 */

#include "hal/hmi_hal.h"

#if defined(WITH_HAL_DUMMY)

ret_t hmi_hal_init(void) {
  log_debug("hmi_hal_init\n");
  return RET_OK;
}

ret_t hmi_hal_save_settings(const char* app_name, const void* data, uint32_t size) {
  log_debug("hmi_hal_save_settings\n");
  return RET_FAIL;
}

ret_t hmi_hal_load_setttings(const char* app_name, void** data, uint32_t* size) {
  log_debug("hmi_hal_load_setttings\n");
  return RET_FAIL;
}

ret_t hmi_hal_beep_on(uint32_t nms) {
  log_debug("beep on: %u\n", nms);
  return RET_OK;
}

static uint32_t s_backlight = 60;

ret_t hmi_hal_set_backlight(uint32_t value) {
  value = tk_clampi(value, 0, 100);
  s_backlight = value;
  log_debug("set backlight: %u\n", s_backlight);
  return RET_OK;
}

uint32_t hmi_hal_get_backlight(void) {
  return s_backlight;
}

ret_t hmi_hal_set_rtc(date_time_t* dt) {
  return_value_if_fail(dt != NULL, RET_BAD_PARAMS);
  log_debug("set rtc: %d-%d-%d %d:%d:%d\n", dt->year, dt->month, dt->day, dt->hour, dt->minute,
            dt->second);

  return RET_OK;
}

ret_t hmi_hal_get_rtc(date_time_t* dt) {
  return_value_if_fail(dt != NULL, RET_BAD_PARAMS);

  memset(dt, 0x00, sizeof(date_time_t));
  
  return RET_FAIL;
}

ret_t hmi_hal_play_audio(const char* file) {
  return_value_if_fail(file != NULL, RET_BAD_PARAMS);
  log_debug("play audio: %s\n", file);
  return RET_OK;
}

ret_t hmi_hal_stop_audio(void) {
  log_debug("stop audio\n");
  return RET_OK;
}

static uint32_t s_audio_volume = 60;
ret_t hmi_hal_set_audio_volume(int volume) {
  s_audio_volume = tk_clampi(volume, 0, 100);
  log_debug("set audio volume: %u\n", s_audio_volume);
  return RET_OK;
}

uint32_t hmi_hal_get_audio_volume(void) {
  return s_audio_volume;
}

ret_t hmi_hal_deinit(void) {
  return RET_OK;
}

#endif /*TK_IS_PC*/
